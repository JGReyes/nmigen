
![Programa_PC](https://1.bp.blogspot.com/-rtRD4KSqs80/XmDZxT0eBzI/AAAAAAAACG0/nqw7QaFj410RjwOYPCN8LtLIIRWx1TmbgCKgBGAsYHg/s640/fpga.jpg)

Código de los ejercicios realizados a la hora de empezar con [nMigen](https://github.com/m-labs/nmigen) para el diseño de circuitos en FPGAs. Se incluyen tareas de VS Code para agilizar y no tener que introducir tantos comandos por consola.

Se ha utilizado la placa de desarrollo [iCE40UP5K-B-EVN.](http://www.latticesemi.com/en/Products/DevelopmentBoardsAndKits/iCE40UltraPlusBreakoutBoard)

Son los ejercicios del [tutorial](https://github.com/Obijuan/open-fpga-verilog-tutorial/wiki/Cap%C3%ADtulo-0%3A-you-are-leaving-the-privative-sector) de Verilog de Juan González Gómez (Obijuan), pero realizados con nMigen.

Para más información consultar el [blog.](https://www.circuiteando.net/2020/03/comienzos-con-nmigen.html)
