# -*- coding: utf-8 -*-
# File: counter4_formal.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, January 21st 2020, 3:41:26 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.asserts import Assert, Assume, Cover
from nmigen.asserts import Past, Rose, Fell, Stable, Initial
from nmigen.cli import main_parser, main_runner
from counter4 import Counter4


if __name__ == "__main__":
    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.counter4 = counter4 = Counter4(N=1)

    # Busca casos en los que el valor del contador sea 0, durante los 40 pasos
    # o ciclos, que están cofigurados en el archivo formal.sby
    # Por lo que se creara un archivo trace.vcd con el primer caso en el que el
    # contador es 0. Así como también mostraría el caso de desbordamiento
    # si en los ciclos especificados se llegara a producir.
    m.d.comb += Cover(counter4.data == 0)
    with m.If(Past(counter4.data) != 0):
        m.d.comb += Cover(counter4.data == 0)

    # Prueba que el valor actual sea igual al valor del ciclo anterior + 1.
    # Siempre que el valor sea mayor que 0.
    with m.If(counter4.data > 0):
        m.d.comb += Assert(counter4.data == Past(counter4.data)+1)
    # Prueba que el valor anterior al 0 fué el 15.
    with m.Elif(counter4.data == 0):
        # Excluyendo el ciclo inicial.
        with m.If(Past(counter4.data) != 0):
            m.d.comb += Assert(Past(counter4.data) == 15)
    # Prueba que el contador no utiliza números negativos.
    with m.Else():
        m.d.comb += Assert(1 == 0)  # Fuerza el error.

    main_runner(parser, args, m, ports=counter4.ports())
