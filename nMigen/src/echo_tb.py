# -*- coding: utf-8 -*-
# File: echo_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Monday, February 10th 2020, 4:20:59 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from echo import Echo
from baudgen import *

# Simulación
if __name__ == "__main__":

    # Baudios con los que realizar la simulación
    BAUD = B115200
    # Tics de reloj para envio de datos a esa velocidad

    BITRATE = BAUD

    # Tics necesarios para enviar una trama serie completa, mas un bit adicional
    FRAME = BITRATE * 10
    # Tiempo entre dos bits enviados
    FRAME_WAIT = BITRATE * 4

    m = Module()
    m.submodules.echo = echo = Echo(BAUD=BAUD)

    rx = Signal(reset=1)
    m.d.comb += echo.rx.eq(rx)

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    def process():
        scar = Signal(8)
        # Se envía 0x55
        yield scar.eq(0x55)

        yield rx.eq(0)  # -- Bit start

        for i in range(8):     # -- Bits del 0 al 7
            for _ in range(BITRATE):
                yield Tick()
            yield rx.eq(scar[i])

        for _ in range(BITRATE):
            yield Tick()
        yield rx.eq(1)  # -- Bit stop

        for _ in range(BITRATE):  # -- Esperar a que se envie bit de stop
            yield Tick()

        for _ in range(FRAME_WAIT * 3):
            yield Tick()

        # Se envía K
        yield scar.eq(ord("K"))

        yield rx.eq(0)  # -- Bit start

        for i in range(8):     # -- Bits del 0 al 7
            for _ in range(BITRATE):
                yield Tick()
            yield rx.eq(scar[i])

        for _ in range(BITRATE):
            yield Tick()
        yield rx.eq(1)  # -- Bit stop

        for _ in range(BITRATE):  # -- Esperar a que se envie bit de stop
            yield Tick()

    sim.add_sync_process(process)

    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/echo_tb.vcd", traces=echo.ports()):
        sim.run_until(400e-6, run_passive=True)  # Simula durante 400 uS.
