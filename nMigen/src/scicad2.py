# -*- coding: utf-8 -*-
# File: scicad2.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, February 7th 2020, 3:09:09 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from baudgen import *
from uart_tx import Uart_tx
from divider2 import *


class Scicad2(Elaboratable):
    def __init__(self, BAUD=B115200, DELAY=T_1s):
        self.tx = Signal()    # -- Salida de datos serie

        self.baud = BAUD
        self.delay = DELAY

    def elaborate(self, platform):
        m = Module()

        # Señal de listo del transmisor serie
        ready = Signal()
        # Dato a transmitir (normal y registrado)
        data = Signal(8)
        data_r = Signal.like(data)

        # Señal para indicar al controlador el comienzo de la transmision
        # de la cadena.
        transmit = Signal()
        transmit_r = Signal.like(transmit)

        # Microordenes
        # Counter enable (cuando cena = 1)
        cena = Signal()
        # Transmitir cadena (cuando transmit = 1)
        start = Signal()

        # -------------------------------------
        # -- RUTA DE DATOS
        # -------------------------------------

        # Instanciar la Unidad de transmision
        m.submodules.tx0 = tx0 = Uart_tx(BAUD=self.baud)
        m.d.comb += tx0.data.eq(data_r)
        m.d.comb += tx0.start.eq(start)
        m.d.comb += ready.eq(tx0.ready)
        m.d.comb += self.tx.eq(tx0.tx)

        # -- Contador de caracteres
        # -- Cuando la microorden cena esta activada, se incrementa
        car_count = Signal(3)

        # -- Multiplexor con los caracteres de la cadena a transmitir
        # -- se seleccionan mediante la señal car_count

        with m.Switch(car_count):
            with m.Case(0):
                m.d.comb += data.eq(ord("H"))
            with m.Case(1):
                m.d.comb += data.eq(ord("o"))
            with m.Case(2):
                m.d.comb += data.eq(ord("l"))
            with m.Case(3):
                m.d.comb += data.eq(ord("a"))
            with m.Case(4):
                m.d.comb += data.eq(ord("!"))
            with m.Case(5, 6, 7):
                m.d.comb += data.eq(ord("."))
            with m.Default():
                m.d.comb += data.eq(ord("."))

        # Registrar los datos de salida del multiplexor
        m.d.sync += data_r.eq(data)

        # Registrar señal para cumplir con normas diseño sincrono
        m.d.sync += transmit_r.eq(transmit)

        with m.If(ResetSignal() == 1):
            m.d.sync += car_count.eq(0)
        with m.Elif(cena):
            m.d.sync += car_count.eq(car_count+1)

        # Temporizador
        m.submodules.div0 = div0 = Divider2(M=self.delay, PULSE=True)
        m.d.comb += transmit.eq(div0.clk_out)

        # -------------------------------------
        # -- CONTROLADOR
        # -------------------------------------

        with m.FSM(reset="IDLE") as fsm:
            # Estado inicial. Se sale de este estado al recibirse la
            # señal de transmit, conectada al DTR.
            with m.State("IDLE"):
                m.d.comb += start.eq(0)
                m.d.comb += cena.eq(0)
                with m.If(transmit == 1):
                    m.next = "TXCAR"
            # Estado de transmision de un caracter. Esperar a que el
            # transmisor serie este disponible. Cuando lo esta se pasa al
            # siguiente estado
            with m.State("TXCAR"):
                m.d.comb += start.eq(1)
                m.d.comb += cena.eq(0)
                with m.If(ready == 1):
                    m.next = "NEXT"
            # Envio del siguiente caracter. Es un estado transitorio
            # Cuando se llega al último caracter se pasa para finalizar
            # la transmision
            with m.State("NEXT"):
                m.d.comb += start.eq(0)
                m.d.comb += cena.eq(1)
                with m.If(car_count == 7):
                    m.next = "END"
                with m.Else():
                    m.next = "TXCAR"
            # Último estado:finalizacion de la transmision. Se espera hasta
            # que se haya enviado el ultimo caracter. Cuando ocurre se vuelve
            # al estado de reposo inicial
            with m.State("END"):
                m.d.comb += start.eq(0)
                m.d.comb += cena.eq(0)
                # Esperar a que se termine último caracter
                with m.If(ready == 1):
                    m.next = "IDLE"

        return m

    def ports(self):
        return [self.tx]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":

    m = Module()

    subModulo = Scicad2()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    # Como se utiliza el dominio síncrono por defecto, y en la configuración
    # de la placa se indica el reloj por defecto. No hace falta indicarselo ahora.
    m.d.comb += platform.request("uart").tx.eq(subModulo.tx)  # 38B Header B

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
