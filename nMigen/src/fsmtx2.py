# -*- coding: utf-8 -*-
# File: fsmtx2.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, February 6th 2020, 7:30:58 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from baudgen import *
from divider2 import *


class Fsmtx2(Elaboratable):
    def __init__(self, BAUD=B115200, CAR="A", DELAY=T_100ms):
        self.tx = Signal()

        self.baud = BAUD
        self.car = CAR
        self.delay = DELAY

    def elaborate(self, platform):
        m = Module()
        # Generador de reloj para los baudios.
        m.submodules.baud0 = baud0 = Baudgen(M=self.baud)

        # Divisor de 1 pulso.
        m.submodules.div0 = div0 = Divider2(M=self.delay, PULSE=True)

        # -- Registro de 10 bits para almacenar la trama a enviar:
        # -- 1 bit start + 8 bits datos + 1 bit stop
        shifter = Signal(10)

        start = Signal()
        # Señal start registrada
        start_r = Signal()

        # Bitcounter
        bitc = Signal(4)

        # Microordenes
        # Carga del registro de desplazamiento. Puesta a 0 del contador de bits.
        load = Signal()
        # Habilita el generador de baudios para la transmision.
        baud_en = Signal()

        # -------------------------------------
        # -- RUTA DE DATOS
        # -------------------------------------

        # Registra la entrada start
        m.d.sync += start_r.eq(start)

        # -- Registro de desplazamiento, con carga paralela
        # -- Cuando load es 0, se carga la trama
        # -- Cuando load es 1 se desplaza hacia la derecha, y se
        # -- introducen '1's por la izquierda
        # También se saca por tx el bit menos significativo del registros de
        # desplazamiento y se mantiene a 1 mientras se realiza la carga.

        # Trama
        start_bit = Signal()
        stop_bit = Signal(reset=1)
        send_byte = Signal(8, reset=ord(self.car))

        # Se utiliza la señal por defecto de reset que crea nMigen para el dominio sync.
        with m.If(ResetSignal() == 1):
            m.d.sync += shifter.eq(0b11_1111_1111)
        with m.Elif(load == 1):
            m.d.sync += self.tx.eq(1)
            m.d.sync += shifter.eq(Cat(start_bit, send_byte, stop_bit))
            m.d.sync += bitc.eq(0)  # Contador de bits a 0.
        with m.Elif(baud0.clk_out == 1):
            m.d.sync += self.tx.eq(shifter[0])
            m.d.sync += shifter.eq(Cat(shifter[1:], 0b1))
            m.d.sync += bitc.eq(bitc+1)  # Aumenta los bits enviados.

        # Se conecta el habilitador del generador de baudios a la microorden.
        m.d.comb += baud0.clk_ena.eq(baud_en)
        # Se conecta la señal de start al divisor de 1 pulso.
        m.d.comb += start.eq(div0.clk_out)

        # -------------------------------------
        # -- CONTROLADOR
        # -------------------------------------

        with m.FSM(reset="IDLE") as fsm:
            # Estado de reposo. Se sale cuando la señal
            # de start se pone a 1.
            with m.State("IDLE"):
                m.d.comb += load.eq(0)
                m.d.comb += baud_en.eq(0)
                with m.If(start_r == 1):
                    m.next = "START"
            # Estado de comienzo. Prepararse para empezar
            # a transmitir. Duracion: 1 ciclo de reloj.
            with m.State("START"):
                m.d.comb += load.eq(1)
                m.d.comb += baud_en.eq(1)
                m.next = "TRANS"
            # Transmitiendo. Se esta en este estado hasta
            # que se hayan transmitido todos los bits pendientes.
            with m.State("TRANS"):
                m.d.comb += load.eq(0)
                m.d.comb += baud_en.eq(1)
                with m.If(bitc == 11):
                    m.next = "IDLE"

        # Inicializador
        # En principio no haría falta, ya que se ha utilizado el que genera nMiguen
        # y el se encarga de inicializar los resets de los dominios síncronos.

        return m

    def ports(self):
        return [self.tx]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":

    m = Module()

    subModulo = Fsmtx2()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    m.d.comb += platform.request("uart").tx.eq(subModulo.tx)  # 38B Header B

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
