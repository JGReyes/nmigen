# -*- coding: utf-8 -*-
# File: tones.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Wednesday, January 29th 2020, 4:39:15 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from divider import *


class Tones(Elaboratable):
    def __init__(self, F0=F_1KHz, F1=F_2KHz, F2=F_3KHz, F3=F_4KHz):
        self.ch0 = Signal()
        self.ch1 = Signal()
        self.ch2 = Signal()
        self.ch3 = Signal()
        self.clk = Signal()

        self.f0 = F0
        self.f1 = F1
        self.f2 = F2
        self.f3 = F3

    def elaborate(self, platform):
        m = Module()

        # Generadores de tono.
        m.submodules.div0 = div0 = Divider(M=self.f0)
        m.submodules.div1 = div1 = Divider(M=self.f1)
        m.submodules.div2 = div2 = Divider(M=self.f2)
        m.submodules.div3 = div3 = Divider(M=self.f3)

        # Se conectan las entradas de reloj.
        m.d.comb += div0.clk_in.eq(self.clk)
        m.d.comb += div1.clk_in.eq(self.clk)
        m.d.comb += div2.clk_in.eq(self.clk)
        m.d.comb += div3.clk_in.eq(self.clk)

        # Se conectan las salidas.
        m.d.comb += self.ch0.eq(div0.clk_out)
        m.d.comb += self.ch1.eq(div1.clk_out)
        m.d.comb += self.ch2.eq(div2.clk_out)
        m.d.comb += self.ch3.eq(div3.clk_out)

        return m

    def ports(self):
        return [self.ch0, self.ch1, self.ch2, self.ch3, self.clk]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":

    m = Module()
    subModulo = Tones()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    m.d.comb += subModulo.clk.eq(platform.request("clk12"))
    # Sólo se conecta una salida y se comprueba con un analizador lógico.
    m.d.comb += platform.request("rgb_led").r.eq(subModulo.ch3)

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
