# -*- coding: utf-8 -*-
# File: genram.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, February 14th 2020, 6:23:57 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
import string
import os


class Genram(Elaboratable):
    def __init__(self,  AW=5,  # -- Bits de las direcciones (Adress width)
                 DW=4,  # -- Bits de los datos (Data witdh)
                 ROMFILE="bufferini.list"):  # -- Fichero con el contenido de la RAM

        self.addr = Signal(AW)     # Direcciones
        self.data_in = Signal(DW)  # Dato de entrada
        self.data_out = Signal(DW)  # Dato a escribir
        self.rw = Signal()         # Modo lectura (1) o escritura (0)

        self.aw = AW
        self.dw = DW
        self.romfile = ROMFILE

    def _loadfile(self):
        # Abre el fichero especificado y lee todas las líneas.
        fichero = ''.join([os.getcwd(), os.sep, "src", os.sep, self.romfile])
        with open(fichero, "r") as file:
            lineas = file.readlines()
            # Contenido de la memoria
            # Si la linea es un comentario no se tiene en cuenta.
            # La línea se convierte a entero, asumiendo que el número está en hexadecimal.
            try:
                data = [int(linea.strip(), 16)
                        for linea in lineas if (linea[0] != "#")]
            except ValueError:
                print(
                    f"Error de conversión, la rom: {fichero} contiene un número no hexadecimal.")
                os._exit(1)

            return data

    def elaborate(self, platform):
        m = Module()

        # -- Calcular el numero de posiciones totales de memoria
        NPOS = 2 ** self.aw

        # Cargar en la memoria el fichero ROMFILE
        # Los valores deben estan dados en hexadecimal
        data = self._loadfile()

        mem = Memory(width=self.dw, depth=NPOS, init=data)

        # Como es memoria RAM, se le asignan los dos puertos.
        m.submodules.read_port = read_port = mem.read_port()
        m.d.comb += [read_port.addr.eq(self.addr),
                     self.data_out.eq(read_port.data)]
        m.submodules.write_port = write_port = mem.write_port()
        m.d.comb += [write_port.addr.eq(self.addr),
                     write_port.data.eq(self.data_in),
                     write_port.en.eq(~self.rw)]  # La memoria utiliza lógica inversa.

        return m

    def ports(self):
        return [self.addr, self.data_in, self.data_out, self.rw]
