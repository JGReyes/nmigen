# -*- coding: utf-8 -*-
# File: microbio_formal.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Wednesday, February 26th 2020, 10:51:37 am
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

# Algunas comprobaciones formales para el microbio.

from nmigen import *
from nmigen.asserts import Assert, Assume, Cover
from nmigen.asserts import Past, Rose, Fell, Stable
from nmigen.cli import main_parser, main_runner
from microbio import Microbio


if __name__ == "__main__":
    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    # micro_test.list contiene el programa M1.asm ensamblado, y se usa para verificar
    # el funcionamiento de microbio.
    m.submodules.micro = micro = Microbio(
        WAIT_DELAY=4, ROMFILE="micro_test.list", VERIFICATION=True)

    act_cycle = Signal(range(48))  # Contador para los ciclos de reloj
    m.d.sync += act_cycle.eq(act_cycle+1)

    inst_count = Signal(range(16))  # Contador de instrucciones.

    # Para asegurarse de no pasarse de los ciclos que puede contar el contador.
    m.d.comb += Assume(act_cycle < 48)
    # Se asume que la línea de reset no está activa.
    m.d.comb += Assume(micro.rstn_ini == 1)

    # Se comprueba los valores iniciales en el ciclo 0
    with m.If(act_cycle == 0):
        m.d.comb += Assert(micro.f_addr == 0)
        m.d.comb += Assert(micro.f_co == 0)
        m.d.comb += Assert(micro.f_cp == 0)
        m.d.comb += Assert(micro.f_cp_inc == 0)
        m.d.comb += Assert(micro.f_cp_load == 0)
        m.d.comb += Assert(micro.f_dat == 0)
        m.d.comb += Assert(micro.f_halt == 0)
        m.d.comb += Assert(micro.f_leds_load == 0)
        m.d.comb += Assert(micro.f_ri == 0)
        m.d.comb += Assert(micro.f_ri_load == 0)

    # Comprueba que el contador se incrementa al cargar un dato.
    # Y se activó la microorden en el ciclo anterior.
    with m.If((act_cycle > 1) & Past(micro.f_ri_load)):
        m.d.comb += Assert(Past(micro.f_cp) == micro.f_cp-1)
        m.d.comb += Assert(Past(micro.f_cp_inc))

    # Comprueba la descomposición de las instrucciones.
    m.d.comb += Assert(micro.f_co == micro.f_ri[6:])
    m.d.comb += Assert(micro.f_dat == micro.f_ri[:6])

    # Comprueba que se cargan las instrucciones, y sea una de las utilizadas
    # por el programa.
    with m.If(Past(micro.f_ri_load)):
        m.d.comb += Assert(micro.f_ri == micro.f_data)
        m.d.comb += Assert((micro.f_ri == 0x00) | (micro.f_ri == 0x81) |
                           (micro.f_ri == 0x82) | (micro.f_ri == 0x84) |
                           (micro.f_ri == 0x88) | (micro.f_ri == 0x40))
        # Aumenta el contador.
        m.d.sync += inst_count.eq(inst_count+1)

    # Comprueba que cuando se active la señal de parada, la instrución sea
    # la correspondiente y se hayan ejecutado todas las instrucciones.
    with m.If(Past(micro.f_halt)):
        m.d.comb += Assert(micro.f_ri == 0x40)
        m.d.comb += Assert(micro.f_co == 0b01)
        # Parada en la décima instrucción.
        m.d.comb += Assert(inst_count == 0xA)

    # Comprueba la instrucción LEDS
    with m.If(Past(micro.f_leds_load)):
        m.d.comb += Assert(micro.f_co == 0b10)
        m.d.comb += Assert(micro.leds == micro.f_dat[:4])

    # Comprueba la instrucción JP
    with m.If(Past(micro.f_cp_load)):
        m.d.comb += Assert(micro.f_co == 0b11)
        m.d.comb += Assert(micro.f_addr == micro.f_dat)

    # En el programa de prueba no hay instrucción JP, por lo que no debería de activarse.
    m.d.comb += Assert(micro.f_cp_load == 0)

    # Limita las direcciones de memoria e instrucciones durante la fase de inducción.
    m.d.comb += Assume(micro.f_addr < 11)
    m.d.comb += Assume(inst_count < 11)
    m.d.comb += Assume((micro.f_data == 0x00) | (micro.f_data == 0x81) |
                       (micro.f_data == 0x82) | (micro.f_data == 0x84) |
                       (micro.f_data == 0x88) | (micro.f_data == 0x40))

    main_runner(parser, args, m, ports=micro.ports())
