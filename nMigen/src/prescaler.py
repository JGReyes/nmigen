# -*- coding: utf-8 -*-
# File: prescaler.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, January 16th 2020, 5:58:01 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform


class Prescaler(Elaboratable):
    # N es el número de bits del prescaler. Por defecto 25.
    def __init__(self, N=25):
        self.clk_in = Signal()
        self.clk_out = Signal()
        self.n = N

    def elaborate(self, platform):
        m = Module()
        # Almacena el valor del contador de n bits.
        count = Signal(self.n)
        # El dominio síncrono está controlado por la entrada clk_in.
        m.domains.sync = ClockDomain()
        m.d.comb += ClockSignal().eq(self.clk_in)
        # Aumenta el contador.
        m.d.sync += count.eq(count+1)
        # Asigna el bit más significativo a la salida.
        m.d.comb += self.clk_out.eq(count[-1])

        return m

    def ports(self):
        return [self.clk_in, self.clk_out]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":

    subModulo = Prescaler(N=25)

    m = Module()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    m.d.comb += subModulo.clk_in.eq(platform.request("clk12"))
    m.d.comb += platform.request("rgb_led").g.eq(subModulo.clk_out)

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
