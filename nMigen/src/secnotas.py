# -*- coding: utf-8 -*-
# File: secnotas.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, February 4th 2020, 12:55:33 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from divider import *


class Secnotas(Elaboratable):
    def __init__(self, N0=DO_4, N1=RE_4, N2=MI_4, DUR=T_250ms):
        self.ch_out = Signal()
        self.clk = Signal()

        self.n0 = N0
        self.n1 = N1
        self.n2 = N2
        self.dur = DUR

    def elaborate(self, platform):
        m = Module()

        # Generadores de tono.
        m.submodules.ch0 = ch0 = Divider(M=self.n0)
        m.submodules.ch1 = ch1 = Divider(M=self.n1)
        m.submodules.ch2 = ch2 = Divider(M=self.n2)
        # Temporizador
        m.submodules.timer0 = timer0 = Divider(M=self.dur)

        # Se conectan las entradas de reloj.
        m.d.comb += ch0.clk_in.eq(self.clk)
        m.d.comb += ch1.clk_in.eq(self.clk)
        m.d.comb += ch2.clk_in.eq(self.clk)
        m.d.comb += timer0.clk_in.eq(self.clk)

        # Dominio síncrono para el contador.
        m.domains.cont = ClockDomain("cont")
        m.d.comb += ClockSignal("cont").eq(timer0.clk_out)

        # Contador
        sel = Signal(2)
        m.d.cont += sel.eq(sel+1)

        # Multiplexor
        with m.Switch(sel):
            with m.Case(0):
                m.d.comb += self.ch_out.eq(ch0.clk_out)
            with m.Case(1):
                m.d.comb += self.ch_out.eq(ch1.clk_out)
            with m.Case(2):
                m.d.comb += self.ch_out.eq(ch2.clk_out)
            with m.Case(3):
                m.d.comb += self.ch_out.eq(0)
            with m.Default():
                m.d.comb += self.ch_out.eq(0)

        return m

    def ports(self):
        return [self.ch_out, self.clk]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":

    m = Module()
    subModulo = Secnotas()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    m.d.comb += subModulo.clk.eq(platform.request("clk12"))
    # Se conecta la salida al led rojo y se comprueba la frencuencia con un analizador lógico.
    m.d.comb += platform.request("rgb_led").r.eq(subModulo.ch_out)

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
