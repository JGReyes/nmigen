# -*- coding: utf-8 -*-
# File: romnotes.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, February 11th 2020, 8:14:41 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from notegen import Notegen
from genrom import Genrom
from divider2 import *


class Romnotes(Elaboratable):
    def __init__(self, DUR=T_200ms, ROMFILE="imperial.list", AW=6, DW=16):
        self.leds = Signal(5)
        self.ch_out = Signal()

        self.dur = DUR
        self.romfile = ROMFILE
        self.aw = AW
        self.dw = DW

    def elaborate(self, platform):
        m = Module()

        # Selección del canal del multiplexor
        addr = Signal(self.aw)

        # Instancia de la memoria rom
        m.submodules.rom = rom = Genrom(
            AW=self.aw, DW=self.dw, ROMFILE=self.romfile)
        m.d.comb += rom.addr.eq(addr)

        # Generador de notas
        m.submodules.ch0 = ch0 = Notegen()
        m.d.comb += ch0.note.eq(rom.data)
        m.d.comb += self.ch_out.eq(ch0.clk_out)

        # Sacar los 5 bits menos significativos de la nota por los leds
        m.d.comb += self.leds.eq(rom.data[:5])

        # Divisor para marcar la duración de cada nota
        m.submodules.timer0 = timer0 = Divider2(M=self.dur, PULSE=True)

        # Contador para seleccion de nota
        with m.If(timer0.clk_out):
            m.d.sync += addr.eq(addr+1)

        return m

    def ports(self):
        return [self.leds, self.ch_out]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":
    m = Module()

    subModulo = Romnotes()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    # Como se utiliza el dominio síncrono por defecto, y en la configuración
    # de la placa se indica el reloj por defecto. No hace falta indicarselo ahora.
    # Invierte el orden de los leds para que MSB sea el rojo.
    m.d.comb += platform.request("rgb_led").eq(subModulo.leds[::-1])
    # Pin tx en 38B Hearder B
    m.d.comb += platform.request("uart").tx.eq(subModulo.ch_out)

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
