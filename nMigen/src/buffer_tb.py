# -*- coding: utf-8 -*-
# File: buffer_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Monday, February 17th 2020, 11:37:26 am
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick, Passive
from buffer import Buffer
from baudgen import *

# Simulación
if __name__ == "__main__":

    ROMFILE = "bufferini.list"

    # Baudios con los que realizar la simulación
    BAUD = 2
    # Tics de reloj para envio de datos a esa velocidad
    BITRATE = BAUD

    # Tics necesarios para enviar una trama serie completa, mas un bit adicional
    FRAME = BITRATE * 11
    # Tiempo entre dos bits enviados
    FRAME_WAIT = BITRATE * 4

    m = Module()
    m.submodules.buffer = buffer = Buffer(BAUD=BAUD, ROMFILE=ROMFILE)

    # Simula la señal rx
    rx = Signal()
    m.d.comb += buffer.rx.eq(rx)

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    car = Signal(8)

    def process():
        cadena = "HOLA QUE HACES-."
        for _ in range(FRAME_WAIT * 46):
            yield Tick()

        for i in range(len(cadena)):
            yield car.eq(ord(cadena[i]))  # Almacena cada carácter.
            # Manda el carácter.
            yield rx.eq(0)  # -- Bit start
            for i in range(10):
                for _ in range(BITRATE):
                    yield Tick()
                if (i <= 7):  # -- Bits del 0 al 7
                    yield rx.eq(car[i])
                elif (i == 8):  # -- Bit stop
                    yield rx.eq(1)
                else:  # -- Esperar a que se envie bit de stop
                    yield Tick()

            for _ in range(FRAME_WAIT * 2):
                yield Tick()

        for _ in range(FRAME_WAIT * 46):
            yield Tick()
        print("FIN de la simulación")

    sim.add_sync_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/buffer_tb.vcd", traces=buffer.ports()):
        sim.run()
