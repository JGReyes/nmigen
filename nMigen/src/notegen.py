# -*- coding: utf-8 -*-
# File: notegen.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, February 11th 2020, 8:14:14 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *


class Notegen(Elaboratable):
    def __init__(self):
        self.note = Signal(16)
        self.clk_out = Signal()

    def elaborate(self, platform):
        m = Module()

        # Registro para implementar el contador modulo note
        divcounter = Signal(16)

        clk_temp = Signal()

        # Contador módulo note
        with m.If(ResetSignal() == 1):
            m.d.sync += divcounter.eq(0)
        # Si la nota es 0 no se incrementa contador
        with m.Elif(self.note == 0):
            m.d.sync += divcounter.eq(0)
        # Si se alcanza el tope, poner a 0
        with m.Elif(divcounter == self.note-1):
            m.d.sync += divcounter.eq(0)
        # Incrementar contador
        with m.Else():
            m.d.sync += divcounter.eq(divcounter+1)

        # Sacar un pulso de anchura 1 ciclo de reloj si el generador
        with m.If(divcounter == 0):
            m.d.comb += clk_temp.eq(1)
        with m.Else():
            m.d.comb += clk_temp.eq(0)

        # Divisor de frecuencia entre 2, para obtener como salida una señal
        # con un ciclo de trabajo del 50%
        with m.If(ResetSignal() == 1):
            m.d.sync += self.clk_out.eq(0)
        with m.Elif(self.note == 0):
            m.d.sync += self.clk_out.eq(0)
        with m.Elif(clk_temp == 1):
            m.d.sync += self.clk_out.eq(~self.clk_out)

        return m

    def ports(self):
        return [self.note, self.clk_out]
