# -*- coding: utf-8 -*-
# File: blink4_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Wednesday, January 22nd 2020, 8:19:46 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from blink4 import Blink4

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.blink4 = blink4 = Blink4(N=1)

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    def clock_gen():
        yield blink4.clk.eq(0)
        for _ in range(100):
            yield blink4.clk.eq(~blink4.clk)

    sim.add_process(clock_gen)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/blink4_tb.vcd", traces=blink4.ports()):
        sim.run_until(50e-6, run_passive=True)  # Simula durante 50 uS.
