# -*- coding: utf-8 -*-
# File: counter4_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, January 21st 2020, 12:51:53 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from counter4 import Counter4

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.counter4 = counter4 = Counter4(N=1)

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    counter_check = Signal(4)

    def clock_gen():
        yield counter4.clk.eq(0)
        for _ in range(100):
            yield counter4.clk.eq(~counter4.clk)

    def process():
        data = yield counter4.data
        if(data != 0):
            print("ERROR: contador no iniciado a 0.")
        else:
            print("Contador inicializado. OK.")

        while True:
            # NOTA: El simulador está en proceso de cambio y en el futuro
            # se añadirá la capacidad de esperar hasta el cambio de una señal.
            # Lo que se hace es comprobar continuamente data y al cambiar de
            # valor se realiza la comprobación. Hasta que termine el tiempo de simulación.
            old_data = yield counter4.data
            yield Delay(20e-9)  # Se realiza una espera de 20 nS
            data = yield counter4.data

            if (data != old_data):
                yield counter_check.eq(counter_check+1)
                # Se espera a que tenga lugar el aumento de counter_check.
                yield Settle()
                count_check = yield counter_check
                if(data != count_check):
                    print(f"ERROR: Se esperaba {count_check}, \
                          y se obtuvo {data}.")

    sim.add_process(process)
    sim.add_sync_process(clock_gen)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/counter4_tb.vcd", traces=counter4.ports()):
        sim.run_until(50e-6, run_passive=True)  # Simula durante 50 uS.
