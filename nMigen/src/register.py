# -*- coding: utf-8 -*-
# File: register.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Saturday, January 25th 2020, 5:14:44 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

from nmigen import *


class Register(Elaboratable):
    def __init__(self, N=4, INI=0):
        self.dout = Signal(N)
        self.din = Signal.like(self.dout)
        self.clk = Signal()
        self.rst = Signal()

        self.ini = INI

    def elaborate(self, platform):
        m = Module()

        # Se crea un dominio para el propio registro.
        m.domains.reg = ClockDomain(name="reg", local=True)
        m.d.comb += ClockSignal("reg").eq(self.clk)

        with m.If(self.rst == 0):
            m.d.reg += self.dout.eq(self.ini)
        with m.Else():
            m.d.reg += self.dout.eq(self.din)

        return m

    def ports(self):
        return [self.dout, self.din, self.clk, self.rst]
