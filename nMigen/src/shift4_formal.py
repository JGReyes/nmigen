# -*- coding: utf-8 -*-
# File: shift4_formal.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, January 23rd 2020, 5:52:01 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.asserts import Assert, Assume, Cover
from nmigen.asserts import Past, Rose, Fell, Stable
from nmigen.cli import main_parser, main_runner
from shift4 import Shift4


if __name__ == "__main__":
    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.shift4 = shift4 = Shift4(NP=1)

    # La señal de inicio válido se pone a 1 en el primer ciclo.
    inicio_val = Signal()
    m.d.pres += inicio_val.eq(1)

    # Contador de ciclos capaz de contar hasta 100.
    cont_clk = Signal(range(101))
    m.d.pres += cont_clk.eq(cont_clk+1)

    # IMPORTANTE: el contador y la señal de inicio están en el dominio síncrono
    # pres, que es el del registro de desplazamiento. Si se deja el dominio por
    # defecto, cuyo reloj mucho más rápido, los ciclos no coincidirán en las
    # comparaciones hechas más abajo.

    # Antes del primer ciclo, data tiene que ser 0
    with m.If(inicio_val == 0):
        m.d.comb += Assert(shift4.data == 0)

    # Se le indica que no siga más allá de 100 ciclos de reloj, ya que se desbordaría
    # el contador de ciclos.
    m.d.comb += Assume(cont_clk < 101)

    # Comprueba los valores en los primeros ciclos.
    with m.Switch(cont_clk):
        with m.Case(1):
            m.d.comb += Assert(shift4.data == 0b0001)
        with m.Case(2):
            m.d.comb += Assert(shift4.data == 0b0010)
        with m.Case(3):
            m.d.comb += Assert(shift4.data == 0b0100)
        with m.Case(4):
            m.d.comb += Assert(shift4.data == 0b1000)

    # Una vez pasado el ciclo inicial, comprueba que cuando data es 1,
    # su valor anterior fué 0b1000.
    with m.If((shift4.data == 1) & (inicio_val) & (cont_clk > 1)):
        m.d.comb += Assert(Past(shift4.data) == 0b1000)

    main_runner(parser, args, m, ports=shift4.ports())
