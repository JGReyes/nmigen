# -*- coding: utf-8 -*-
# File: notas.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Monday, February 3rd 2020, 6:09:55 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from divider import *


class Notas(Elaboratable):
    def __init__(self, N0=DO_4, N1=RE_4, N2=MI_4, N3=FA_4, N4=SOL_4, N5=LA_4,
                 N6=SI_4, N7=DO_5):
        self.ch0 = Signal()
        self.ch1 = Signal()
        self.ch2 = Signal()
        self.ch3 = Signal()
        self.ch4 = Signal()
        self.ch5 = Signal()
        self.ch6 = Signal()
        self.ch7 = Signal()
        self.clk = Signal()

        self.n0 = N0
        self.n1 = N1
        self.n2 = N2
        self.n3 = N3
        self.n4 = N4
        self.n5 = N5
        self.n6 = N6
        self.n7 = N7

    def elaborate(self, platform):
        m = Module()

        # Generadores de tono.
        m.submodules.div0 = div0 = Divider(M=self.n0)
        m.submodules.div1 = div1 = Divider(M=self.n1)
        m.submodules.div2 = div2 = Divider(M=self.n2)
        m.submodules.div3 = div3 = Divider(M=self.n3)
        m.submodules.div4 = div4 = Divider(M=self.n4)
        m.submodules.div5 = div5 = Divider(M=self.n5)
        m.submodules.div6 = div6 = Divider(M=self.n6)
        m.submodules.div7 = div7 = Divider(M=self.n7)

        # Se conectan las entradas de reloj.
        m.d.comb += div0.clk_in.eq(self.clk)
        m.d.comb += div1.clk_in.eq(self.clk)
        m.d.comb += div2.clk_in.eq(self.clk)
        m.d.comb += div3.clk_in.eq(self.clk)
        m.d.comb += div4.clk_in.eq(self.clk)
        m.d.comb += div5.clk_in.eq(self.clk)
        m.d.comb += div6.clk_in.eq(self.clk)
        m.d.comb += div7.clk_in.eq(self.clk)

        # Se conectan las salidas.
        m.d.comb += self.ch0.eq(div0.clk_out)
        m.d.comb += self.ch1.eq(div1.clk_out)
        m.d.comb += self.ch2.eq(div2.clk_out)
        m.d.comb += self.ch3.eq(div3.clk_out)
        m.d.comb += self.ch4.eq(div4.clk_out)
        m.d.comb += self.ch5.eq(div5.clk_out)
        m.d.comb += self.ch6.eq(div6.clk_out)
        m.d.comb += self.ch7.eq(div7.clk_out)

        return m

    def ports(self):
        return [self.ch0, self.ch1, self.ch2, self.ch3, self.ch4, self.ch5,
                self.ch6, self.ch7, self.clk]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":

    m = Module()
    subModulo = Notas()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    m.d.comb += subModulo.clk.eq(platform.request("clk12"))
    # Se conectan varias notas a una salida y se comprueban con un analizador lógico.
    m.d.comb += platform.request("rgb_led").r.eq(subModulo.ch5)

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
