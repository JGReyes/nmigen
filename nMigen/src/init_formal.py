# -*- coding: utf-8 -*-
# File: init_formal.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, January 23rd 2020, 11:56:15 am
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.asserts import Assert, Assume, Cover
from nmigen.asserts import Past, Rose, Fell, Stable
from nmigen.cli import main_parser, main_runner
from init import Init


if __name__ == "__main__":
    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.init = init = Init()

    # La señal de inicio válido se pone a 1 en el primer ciclo.
    inicio_val = Signal()
    m.d.sync += inicio_val.eq(1)

    # Contador de ciclos capaz de contar hasta 100.
    cont_clk = Signal(range(101))
    m.d.sync += cont_clk.eq(cont_clk+1)

    # Antes del primer ciclo, ini tiene que ser 0
    with m.If(inicio_val == 0):
        m.d.comb += Assert(init.ini == 0)

    # Si ini es 1 y no es el primer ciclo, el pasado de ini siempre tiene
    # que ser 1.
    with m.If((init.ini == 1) & (inicio_val) & (cont_clk > 1)):
        m.d.comb += Assert(Stable(init.ini))

    main_runner(parser, args, m, ports=init.ports())
