# -*- coding: utf-8 -*-
# File: microbio.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, February 25th 2020, 10:18:08 am
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from genrom import Genrom
from divider2 import *
from enum import Enum, unique

# Generar el archivo prog.list a partir del archivo en ensamblador con
# la siguiente instrucción: python3 masm.py -nocomments x.asm


class Microbio(Elaboratable):
    def __init__(self, WAIT_DELAY=T_200ms, ROMFILE="prog.list",
                 VERIFICATION=False):
        self.rstn_ini = Signal()    # Reset
        self.leds = Signal(4)   # Leds
        self.stop = Signal()    # Indicador de stop

        self.wait_delay = WAIT_DELAY  # Tiempo de espera para la instruccion WAIT
        self.romfile = ROMFILE       # Fichero con el programa a cargar en la rom

        # Tamaño de la memoria ROM a instanciar
        self.aw = 6  # Anchura del bus de direcciones
        self.dw = 8  # Anchura del bus de datos

        # Indica si se quiere realizar la verificación formal.
        self.verificación = VERIFICATION

        # Crea las señales para la verificación, todas empiezan por f (formal).
        if (self.verificación):
            self.f_data = Signal(self.dw)
            self.f_addr = Signal(self.aw)
            self.f_cp_load = Signal()
            self.f_cp_inc = Signal()
            self.f_ri_load = Signal()
            self.f_halt = Signal()
            self.f_leds_load = Signal()
            self.f_cp = Signal(self.aw)
            self.f_ri = Signal(self.dw)
            self.f_co = Signal(2)
            self.f_dat = Signal(6)
            self.f_timer_clk = Signal()

    def elaborate(self, platform):

        # Codigo de operación de las instrucciones
        @unique
        class OpCode(Enum):
            WAIT = 0b00
            HALT = 0b01
            LEDS = 0b10
            JP = 0b11

        m = Module()

        # Instancia de la memoria ROM.
        m.submodules.rom = rom = Genrom(
            AW=self.aw, DW=self.dw, ROMFILE=self.romfile)

        # Se conecta con la señal de reset creada por nMigen (reset sin negar)
        # Los registros y señales se ponen en 0 o su valor de reset cuando el
        # reset de nMigen está a 1, por lo que no hace falta ponerlos a 0
        # manualmente cuando rstn_ini está a 0.
        # COMENTAR A LA HORA DE PROGRAMAR LA FPGA
        m.d.comb += ResetSignal().eq(~self.rstn_ini)

        # Declaracion de las microordenes
        cp_inc = Signal()     # Incrementar contador de programa
        cp_load = Signal()    # Cargar el contador de programa
        ri_load = Signal()    # Cargar una instruccion en el registro de instruccion
        halt = Signal()       # Instruccion halt ejecutada
        leds_load = Signal()  # Mostrar un valor por los leds

        # Registro para el contador de programa
        cp = Signal(self.aw)

        # Registro de instruccion
        ri = Signal(self.dw)

        # Descomponer la instruccion en los campos CO y DAT
        co = Signal(2)  # Codigo de operacion
        dat = Signal(6)  # Campo de datos
        m.d.comb += co.eq(ri[6:])
        m.d.comb += dat.eq(ri[:6])

        # Contador de programa
        with m.If(cp_load):
            m.d.sync += cp.eq(dat)
        with m.Elif(cp_inc):
            m.d.sync += cp.eq(cp+1)

        m.d.comb += rom.addr.eq(cp)

        # Registro de instruccion
        with m.If(ri_load):
            m.d.sync += ri.eq(rom.data)

        # Registro de stop
        # Se pone a 1 cuando se ha ejecutado una instruccion de HALT
        reg_stop = Signal()
        with m.If(halt):
            m.d.sync += reg_stop.eq(1)

        # Registro de leds
        leds_r = Signal(4)
        with m.If(leds_load):
            m.d.sync += leds_r.eq(dat[:4])

        m.d.comb += self.leds.eq(leds_r)

        # Mostrar la señal de stop
        m.d.comb += self.stop.eq(reg_stop)

        # Divisor para marcar la duracion de la instrucción WAIT
        m.submodules.timer0 = timer0 = Divider2(M=self.wait_delay, PULSE=True)

        # ----------- UNIDAD DE CONTROL ---------------

        # Valores por defecto
        def init_values():
            m.d.comb += cp_inc.eq(0)
            m.d.comb += cp_load.eq(0)
            m.d.comb += ri_load.eq(0)
            m.d.comb += halt.eq(0)
            m.d.comb += leds_load.eq(0)

        with m.FSM(reset="INIT") as fsm:
            # Estado inicial y de reposo
            with m.State("INIT"):
                init_values()
                m.next = "FETCH"
            # Ciclo de captura: obtener la siguiente instruccion de la memoria
            with m.State("FETCH"):
                init_values()
                # Incrementar CP (en el siguiente estado)
                m.d.comb += cp_inc.eq(1)
                # Cargar la instruccion (en el siguiente estado)
                m.d.comb += ri_load.eq(1)
                m.next = "EXEC"
            # Ciclo de ejecucion
            with m.State("EXEC"):
                # Ejecutar la instrucción
                with m.Switch(co):
                    with m.Case(OpCode.HALT):
                        init_values()
                        m.d.comb += halt.eq(1)  # Activar microorden de halt
                        m.next = "EXEC"  # Permanecer en este estado indefinidamente
                    with m.Case(OpCode.WAIT):
                        init_values()
                        # Mientras no se active clk_out, se sigue en el mismo
                        # estado de ejecucion
                        with m.If(timer0.clk_out):
                            m.next = "FETCH"
                        with m.Else():
                            m.next = "EXEC"
                    with m.Case(OpCode.LEDS):
                        init_values()
                        # Microorden de carga en el registro de leds
                        m.d.comb += leds_load.eq(1)
                        m.next = "FETCH"
                    with m.Case(OpCode.JP):
                        init_values()
                        m.d.comb += cp_load.eq(1)  # Microorden de carga del CP
                        # Realizar un ciclo de reposo para
                        # que se cargue CP antes del estado FETCH
                        m.next = "INIT"

        # Conecta las señales para que sean públicas y se pueda acceder a ellas
        # en el archivo de verificación formal.
        if (self.verificación):
            m.d.comb += self.f_addr.eq(rom.addr)
            m.d.comb += self.f_co.eq(co)
            m.d.comb += self.f_cp.eq(cp)
            m.d.comb += self.f_cp_inc.eq(cp_inc)
            m.d.comb += self.f_cp_load.eq(cp_load)
            m.d.comb += self.f_dat.eq(dat)
            m.d.comb += self.f_data.eq(rom.data)
            m.d.comb += self.f_halt.eq(halt)
            m.d.comb += self.f_leds_load.eq(leds_load)
            m.d.comb += self.f_ri.eq(ri)
            m.d.comb += self.f_ri_load.eq(ri_load)
            m.d.comb += self.f_timer_clk.eq(timer0.clk_out)

        return m

    def ports(self):
        return [self.rstn_ini, self.leds, self.stop]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":
    m = Module()

    subModulo = Microbio()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    # Como se utiliza el dominio síncrono por defecto, y en la configuración
    # de la placa se indica el reloj por defecto. No hace falta indicarselo ahora.
    # Invierte el orden de los leds para que MSB sea el rojo.
    m.d.comb += platform.request("rgb_led").eq(subModulo.leds[::-1])
    m.d.comb += platform.request("pin42b").eq(subModulo.stop)
    # Hay que comentar la asignación de la señal rstn_ini a ResetSignal(),
    # para que se pueda construir, ya que nMigen asigna la señal al reset físico
    # de la placa.

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
