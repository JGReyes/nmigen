# -*- coding: utf-8 -*-
# File: blink4_formal.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Wednesday, January 22nd 2020, 9:25:37 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.asserts import Assert, Assume, Cover
from nmigen.asserts import Past, Rose, Fell, Stable
from nmigen.cli import main_parser, main_runner
from blink4 import Blink4


if __name__ == "__main__":
    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.blink4 = blink4 = Blink4(N=1)

    # La señal past_data se crea con las mismas características que la señal data.
    past_data = Signal.like(blink4.data)
    m.d.comb += past_data.eq(Past(blink4.data))
    # La salida data tiene que ser la negación en su ciclo anterior.
    # Para no contar con el primer ciclo. Solo válido en los ciclos pasados
    # con valor 0xF.
    with m.If(past_data != 0):
        m.d.comb += Assert(blink4.data != past_data)
        m.d.comb += Assert(blink4.data == 0)

    # La salida solo puede ser 0 o 0xF
    m.d.comb += Assert((blink4.data == 0) | (blink4.data == 0xF))

    main_runner(parser, args, m, ports=blink4.ports())
