# -*- coding: utf-8 -*-
# File: prescaler_2.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, January 16th 2020, 5:58:01 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *

# Prescaler_2 es una copia de Prescaler pero sin la entrada de reloj clk_in,
# de modo que comparte el reloj del dominio síncrono por defecto sync.
# Esto hace más facil incluirlo como submódulo donde existen otros con el mismo
# reloj.


class Prescaler(Elaboratable):
    # N es el número de bits del prescaler. Por defecto 25.
    def __init__(self, N=25):
        self.clk_out = Signal()
        self.n = N

    def elaborate(self, platform):
        m = Module()
        # Almacena el valor del contador de n bits.
        count = Signal(self.n)
        # Aumenta el contador.
        m.d.sync += count.eq(count+1)
        # Asigna el bit más significativo a la salida.
        m.d.comb += self.clk_out.eq(count[-1])

        return m

    def ports(self):
        return [self.clk_out]
