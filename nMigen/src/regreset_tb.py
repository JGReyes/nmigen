# -*- coding: utf-8 -*-
# File: regreset_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Saturday, January 25th 2020, 4:20:44 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from regreset import Regreset

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.regreset = regreset = Regreset(NP=1)

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    # En el simuldor se genera el pulso de reloj del prescaler antes de terminar
    # el primer ciclo, lo que pasa el reset a 1 antes de tiempo y
    # no se inicializan los registros. Por lo que, funcionando correctamente
    # en el hardware, para que la simulación se efectue correctamente
    # hay que generar el reset en el segundo ciclo. Para ello sustituir
    # el inicializador en regreset.py con el código siguiente:

    # count = Signal()
    # with m.If(count == 0):
    #     m.d.pres += count.eq(1)
    # with m.If(count == 1):
    # m.d.pres += [reg0.rst.eq(1), reg1.rst.eq(1)]

    def process():
        for _ in range(100):
            yield Tick()    # Pasado un ciclo de reloj.

    sim.add_sync_process(process, domain="pres")
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/regreset_tb.vcd", traces=regreset.ports()):
        sim.run_until(50e-6, run_passive=True)  # Simula durante 50 uS.
