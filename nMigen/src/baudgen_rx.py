# -*- coding: utf-8 -*-
# File: baudgen_rx.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, February 7th 2020, 4:09:51 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *

# Velocidades estándar en baudios.
B115200 = 104
B57600 = 208
B38400 = 313
B19200 = 625
B9600 = 1250
B4800 = 2500
B2400 = 5000
B1200 = 10000
B600 = 20000
B300 = 40000

# Siguiendo las reglas de diseño síncrono y dado que nMigen crea un dominio
# síncrono por defecto (sync) para el reloj principal. Se utiliza éste lo máximo
# posible, eliminando las entradas de reloj de los siguientes circuitos.

# clk_ena: Habilitacion.
#   1. funcionamiento normal. Emitiendo pulsos
#   0: Inicializado y parado. No se emiten pulsos
#
# SALIDAS:
#   clk_out. Señal de salida que marca el tiempo entre bits
#   Anchura de 1 periodo de clk. SALIDA NO REGISTRADA


class Baudgen_rx(Elaboratable):
    def __init__(self, M=B115200):
        self.clk_ena = Signal()
        self.clk_out = Signal()

        self.m = M

    def elaborate(self, platform):
        m = Module()

        # Registro
        # Crea una señal con el número de bits necesarios para el divisor de baudios.
        divcounter = Signal(range(self.m))

        # Valor para llegar a la mitad del periodo
        M2 = (self.m >> 1)

        # Contador de módulo M
        with m.If(self.clk_ena):    # Funcionamiento normal
            with m.If(divcounter == self.m-1):
                m.d.sync += divcounter.eq(0)
            with m.Else():
                m.d.sync += divcounter.eq(divcounter+1)
        with m.Else():              # Contador "congelado" al valor máximo
            m.d.sync += divcounter.eq(self.m-1)

        # Sacar un pulso de anchura 1 ciclo de reloj si el generador
        # esta habilitado (clk_ena == 1)
        # en caso contrario se saca 0.
        with m.If(divcounter == M2):
            m.d.comb += self.clk_out.eq(self.clk_ena)
        with m.Else():
            m.d.comb += self.clk_out.eq(0)

        return m

    def ports(self):
        return [self.clk_ena, self.clk_out]
