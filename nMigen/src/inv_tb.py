# -*- coding: utf-8 -*-
# File: inv_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, January 14th 2020, 4:38:06 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle
from inv import Inv

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.inv = inv = Inv()

# Debido a un bug de nMigen las entradas de los modulos no se exportan al
# archivo vcd. Por lo que hay que crear una señal intermedia.

    A = Signal()
    m.d.comb += inv.A.eq(A)

    sim = Simulator(m)

    def process():
        yield A.eq(0)
        yield Delay(5e-6)  # Esperamos 5 uS
        # fuerza a todas las computaciones combinacionales ha realizarse.
        yield Settle()
        dout = yield inv.B

        if (dout != 0b1):
            print(f"---->¡ERROR! Esperado 1. Leido: {dout}")

        yield A.eq(1)
        yield Delay(5e-6)
        yield Settle()
        dout = yield inv.B

        if (dout != 0b0):
            print(f"---->¡ERROR! Esperado 0. Leido: {dout}")

        yield Delay(5e-6)
        print("FIN de la simulación")
    sim.add_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/inv_tb.vcd", traces=inv.ports()):
        sim.run()
