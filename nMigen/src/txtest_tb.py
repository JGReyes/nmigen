# -*- coding: utf-8 -*-
# File: txtest_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, February 6th 2020, 12:14:55 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from txtest import Txtest
from baudgen import *

# Simulación
if __name__ == "__main__":

    # Baudios con los que realizar la simulación
    BAUD = B115200
    # Tics de reloj para envio de datos a esa velocidad

    BITRATE = BAUD

    # Tics necesarios para enviar una trama serie completa, mas un bit adicional
    FRAME = BITRATE * 11
    # Tiempo entre dos bits enviados
    FRAME_WAIT = BITRATE * 4

    m = Module()
    m.submodules.txtest = txtest = Txtest(BAUD=BAUD)

    # Simula la señal dtr
    dtr = Signal()
    m.d.comb += txtest.load.eq(dtr)

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    def process():
        yield dtr.eq(0)

        # Envia el primer caracter
        for _ in range(FRAME_WAIT):
            yield Tick()

        yield dtr.eq(1)

        for _ in range(FRAME):
            yield Tick()

        yield dtr.eq(0)

        # Segundo envío
        for _ in range(FRAME_WAIT):
            yield Tick()

        yield dtr.eq(1)

        for _ in range(FRAME):
            yield Tick()

        yield dtr.eq(0)

    sim.add_sync_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/txtest_tb.vcd", traces=txtest.ports()):
        sim.run_until(300e-6, run_passive=True)  # Simula durante 300 uS.
