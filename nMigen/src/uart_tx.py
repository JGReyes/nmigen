# -*- coding: utf-8 -*-
# File: uart_tx.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, February 7th 2020, 12:01:56 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from baudgen import *

# --- Modulo de transmision serie
# --- La salida tx ESTA REGISTRADA


class Uart_tx(Elaboratable):
    def __init__(self, BAUD=B115200):
        self.start = Signal()  # -- Activar a 1 para transmitir
        self.data = Signal(8)  # -- Byte a transmitir
        self.tx = Signal()    # -- Salida de datos serie
        self.ready = Signal()  # -- Transmisor listo / ocupado

        self.baud = BAUD

    def elaborate(self, platform):
        m = Module()
        # Generador de reloj para los baudios.
        m.submodules.baud0 = baud0 = Baudgen(M=self.baud)

        # -- Registro de 10 bits para almacenar la trama a enviar:
        # -- 1 bit start + 8 bits datos + 1 bit stop
        shifter = Signal(10)

        # Señal start registrada
        start_r = Signal()

        # Bitcounter
        bitc = Signal(4)

        # Datos regisgrados
        data_r = Signal.like(self.data)

        # Microordenes
        # Carga del registro de desplazamiento. Puesta a 0 del contador de bits.
        load = Signal()
        # Habilita el generador de baudios para la transmision.
        baud_en = Signal()

        # -------------------------------------
        # -- RUTA DE DATOS
        # -------------------------------------

        # Registra la entrada start
        m.d.sync += start_r.eq(self.start)

        # -- Registro de desplazamiento, con carga paralela
        # -- Cuando load es 0, se carga la trama
        # -- Cuando load es 1 se desplaza hacia la derecha, y se
        # -- introducen '1's por la izquierda
        # También se saca por tx el bit menos significativo del registros de
        # desplazamiento y se mantiene a 1 mientras se realiza la carga.

        # Trama
        start = Signal()
        stop = Signal(reset=1)

        # Tx
        tx = Signal(reset=1)
        m.d.comb += self.tx.eq(tx)

        # Se utiliza la señal por defecto de reset que crea nMigen para el dominio sync.
        with m.If(ResetSignal() == 1):
            m.d.sync += shifter.eq(0b11_1111_1111)
        with m.Elif(load == 1):
            m.d.sync += tx.eq(1)
            m.d.sync += shifter.eq(Cat(start, data_r, stop))
            m.d.sync += bitc.eq(0)  # Contador de bits a 0.
        with m.Elif(baud0.clk_out == 1):
            m.d.sync += tx.eq(shifter[0])
            m.d.sync += shifter.eq(Cat(shifter[1:], 0b1))
            m.d.sync += bitc.eq(bitc+1)  # Aumenta los bits enviados.

        # Se conecta el habilitador del generador de baudios a la microorden.
        m.d.comb += baud0.clk_ena.eq(baud_en)

        # -------------------------------------
        # -- CONTROLADOR
        # -------------------------------------

        with m.FSM(reset="IDLE") as fsm:
            # Estado de reposo. Se sale cuando la señal
            # de start se pone a 1.
            with m.State("IDLE"):
                m.d.comb += load.eq(0)
                m.d.comb += baud_en.eq(0)
                m.d.comb += self.ready.eq(1)
                with m.If(start_r == 1):
                    m.d.sync += data_r.eq(self.data)
                    m.next = "START"
            # Estado de comienzo. Prepararse para empezar
            # a transmitir. Duracion: 1 ciclo de reloj.
            with m.State("START"):
                m.d.comb += load.eq(1)
                m.d.comb += baud_en.eq(1)
                m.d.comb += self.ready.eq(0)
                m.next = "TRANS"
            # Transmitiendo. Se esta en este estado hasta
            # que se hayan transmitido todos los bits pendientes.
            with m.State("TRANS"):
                m.d.comb += load.eq(0)
                m.d.comb += baud_en.eq(1)
                m.d.comb += self.ready.eq(0)
                with m.If(bitc == 11):
                    m.next = "IDLE"

        return m

    def ports(self):
        return [self.start, self.data, self.tx, self.ready]
