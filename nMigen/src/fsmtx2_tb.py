# -*- coding: utf-8 -*-
# File: fsmtx2_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, February 7th 2020, 11:33:24 am
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from fsmtx2 import Fsmtx2
from baudgen import *
from divider2 import *

# Simulación
if __name__ == "__main__":

    # Baudios con los que realizar la simulación
    BAUD = B115200
    # Tiempo entre caracteres.
    DELAY = F_8Khz
    # Tics de reloj para envio de datos a esa velocidad
    BITRATE = BAUD
    # Tics necesarios para enviar una trama serie completa, mas un bit adicional
    FRAME = BITRATE * 11
    # Tiempo entre dos bits enviados
    FRAME_WAIT = BITRATE * 4

    m = Module()
    m.submodules.fsmtx2 = fsmtx2 = Fsmtx2(BAUD=BAUD, DELAY=DELAY)

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    def process():
        for _ in range(FRAME_WAIT * 10):
            yield Tick()

    sim.add_sync_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/fsmtx2_tb.vcd", traces=fsmtx2.ports()):
        sim.run_until(500e-6, run_passive=True)  # Simula durante 500 uS.
