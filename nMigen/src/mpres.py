# -*- coding: utf-8 -*-
# File: mpres.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, January 17th 2020, 4:03:21 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from prescaler import Prescaler


class Mpres(Elaboratable):
    # N es el número de bits del prescaler.
    def __init__(self, N0=21, N1=1, N2=2, N3=1, N4=2):
        self.D1 = Signal()
        self.D2 = Signal()
        self.D3 = Signal()
        self.D4 = Signal()
        self.clk_in = Signal()

        self.n0 = N0  # Prescaler base
        self.n1 = N1
        self.n2 = N2
        self.n3 = N3
        self.n4 = N4

    def elaborate(self, platform):
        m = Module()

        m.submodules.pres0 = pres0 = Prescaler(self.n0)
        m.submodules.pres1 = pres1 = Prescaler(self.n1)
        m.submodules.pres2 = pres2 = Prescaler(self.n2)
        m.submodules.pres3 = pres3 = Prescaler(self.n3)
        m.submodules.pres4 = pres4 = Prescaler(self.n4)

        # La entrada del prescaler 0 se conecta al reloj y la salida a las
        # entradas de los demás prescaler.
        # Las salidas de los prescalers se conectan a las salidas D#.
        m.d.comb += [
            pres0.clk_in.eq(self.clk_in),
            pres1.clk_in.eq(pres0.clk_out),
            pres2.clk_in.eq(pres0.clk_out),
            pres3.clk_in.eq(pres0.clk_out),
            pres4.clk_in.eq(pres0.clk_out),

            self.D1.eq(pres1.clk_out),
            self.D2.eq(pres2.clk_out),
            self.D3.eq(pres3.clk_out),
            self.D4.eq(pres4.clk_out),
        ]

        return m

    def ports(self):
        return[self.clk_in, self.D1, self.D2, self.D3, self.D4]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":

    subModulo = Mpres()

    m = Module()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    led = platform.request("rgb_led")
    m.d.comb += subModulo.clk_in.eq(platform.request("clk12"))
    m.d.comb += led.r.eq(subModulo.D1)
    m.d.comb += led.g.eq(subModulo.D2)
    m.d.comb += led.b.eq(subModulo.D3)

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
