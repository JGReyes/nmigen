# -*- coding: utf-8 -*-
# File: setbit_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, January 10th 2020, 4:46:04 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle
from setbit import Setbit

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.setbit = setbit = Setbit()

    sim = Simulator(m)

# Pasadas 10 microsegundos comprobamos si el cable está a '1'
# En caso de no estar a 1, se informa del problema.
    def process():
        yield Delay(10e-6)
        # fuerza a todas las computaciones combinacionales ha realizarse.
        yield Settle()
        # Se pasa el valor de la señal s a una variable de python, para poder
        # compararse directamente.
        salida = yield setbit.A

        if (salida != 1):
            print("---->¡ERROR! Salida no está a 1")
        else:
            print("Componente ok!")

    sim.add_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/setbit_tb.vcd", traces=setbit.ports()):
        sim.run()
