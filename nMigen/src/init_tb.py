# -*- coding: utf-8 -*-
# File: init_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, January 23rd 2020, 11:28:04 am
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from init import Init

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.init = init = Init()

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    def process():
        ini = yield init.ini
        if (ini != 0):
            print("ERROR: Señal ini no iniciada a 0 al inicio.")
        for _ in range(100):
            yield Tick()    # Pasado un ciclo de reloj.
            ini = yield init.ini
            if (ini != 1):
                print("ERROR: Señal ini no mantenida a 1 en los ciclos posteriores.")

    sim.add_sync_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/init_tb.vcd", traces=init.ports()):
        sim.run_until(50e-6, run_passive=True)  # Simula durante 50 uS.
