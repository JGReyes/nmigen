# -*- coding: utf-8 -*-
# File: prescaler_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, January 16th 2020, 5:58:31 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from prescaler import Prescaler

# Simulación
if __name__ == "__main__":
    N = 4     # Número de bits para la simulación.

    m = Module()
    m.submodules.prescaler = prescaler = Prescaler(N=N)
    m.d.comb += prescaler.clk_in.eq(ClockSignal())

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz
    counter_check = Signal(N)

    def process():
        for _ in range(100):   # Cuenta 100 ciclos de reloj
            c_out = yield prescaler.clk_out
            check = yield counter_check[-1]
            if(check != c_out):
                print("--->ERROR! Prescaler no funciona correctamente")
                print(f"Clk out: {c_out}, counter_check[{N-1}]: {check}")
            yield counter_check.eq(counter_check+1)
            # Se necesita un ciclo de reloj para que el cambio tenga efecto.
            yield Tick()

    sim.add_sync_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/prescaler_tb.vcd", traces=prescaler.ports()):
        sim.run()
