# -*- coding: utf-8 -*-
# File: txtest3.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, February 6th 2020, 3:42:48 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from baudgen import *
from divider2 import *

# Siguiendo las reglas de diseño síncrono y dado que nMigen crea un dominio
# síncrono por defecto (sync) para el reloj principal. Se utiliza éste lo máximo
# posible, eliminando las entradas de reloj de los siguientes circuitos.


class Txtest3(Elaboratable):
    def __init__(self, BAUD=B300, DELAY=T_250ms):
        self.tx = Signal()

        self.baud = BAUD
        self.delay = DELAY

    def elaborate(self, platform):
        m = Module()

        load = Signal()
        load_r = Signal()

        # Generador de reloj para los baudios elegidos.
        m.submodules.baud0 = baud0 = Baudgen(M=self.baud)

        # Divisor para generar la señal periodica.
        m.submodules.div0 = div0 = Divider2(M=self.delay)

        # -- Registro de 10 bits para almacenar la trama a enviar:
        # -- 1 bit start + 8 bits datos + 1 bit stop
        shifter = Signal(10)

        # -- Registro de desplazamiento, con carga paralela
        # -- Cuando load es 0, se carga la trama
        # -- Cuando load es 1 se desplaza hacia la derecha, y se
        # -- introducen '1's por la izquierda
        # También se saca por tx el bit menos significativo del registros de
        # desplazamiento y se mantiene a 1 mientras se realiza la carga.

        # Trama
        start = Signal()
        stop = Signal(reset=1)
        k = Signal(8, reset=0x4B)  # K = 0x4B

        with m.If(load_r == 0):  # Modo carga.
            m.d.sync += shifter.eq(Cat(start, k, stop))
            m.d.sync += self.tx.eq(1)  # Señal tx registrada.
        with m.Elif(baud0.clk_out == 1):  # Modo desplazamiento.
            m.d.sync += shifter.eq(Cat(shifter[1:], 0b1))
            m.d.sync += self.tx.eq(shifter[0])  # Señal tx registrada.

        # Señal load registrada.
        m.d.sync += load_r.eq(load)

        # La señal de habilitación se conecta a load registrada.
        m.d.comb += baud0.clk_ena.eq(load_r)

        # La señal de carga se conecta al temporizador.
        m.d.comb += load.eq(div0.clk_out)

        return m

    def ports(self):
        return [self.tx]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":

    m = Module()

    subModulo = Txtest3()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    # Como se utiliza el dominio síncrono por defecto, y en la configuración
    # de la placa se indica el reloj por defecto. No hace falta indicarselo ahora.

    m.d.comb += platform.request("uart").tx.eq(subModulo.tx)  # 38B Header B

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
