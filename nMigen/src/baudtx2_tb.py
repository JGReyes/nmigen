# -*- coding: utf-8 -*-
# File: baudtx2_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Wednesday, February 5th 2020, 6:40:16 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from baudtx2 import Baudtx2

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.baudtx2 = baudtx2 = Baudtx2()

    dtr = Signal()
    m.d.comb += baudtx2.load.eq(dtr)

    m.d.comb += baudtx2.clk.eq(ClockSignal())

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    def process():
        yield dtr.eq(0)
        yield Delay(10e-6)  # 10 microsegundos.
        yield dtr.eq(1)
        yield Delay(400e-6)
        # Segundo envío
        yield dtr.eq(0)
        yield Delay(10e-6)
        yield dtr.eq(1)
        yield Delay(400e-6)
        # Tercer envío
        yield dtr.eq(0)
        yield Delay(10e-6)
        yield dtr.eq(1)
        yield Delay(400e-6)

    sim.add_sync_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/baudtx2_tb.vcd", traces=baudtx2.ports()):
        sim.run_until(1400e-6, run_passive=True)  # Simula durante 1400 uS.
