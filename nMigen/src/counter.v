module counter(output reg [22:0] data,
               input wire clk);

    initial
        data = 0;

    always @(posedge clk) begin
        data <= data + 1;
    end

endmodule