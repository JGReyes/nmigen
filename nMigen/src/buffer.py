# -*- coding: utf-8 -*-
# File: buffer.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, February 14th 2020, 6:57:23 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from genram import Genram
from uart_rx import Uart_rx
from uart_tx2 import Uart_tx
from baudgen import *

# NOTA: se usa uart_tx2, cuya difrencia con uart_tx es que la entrada start
# no está registrada, ya que el ciclo extra ocasionaba la transmisión de un
# byte si, y uno no cuando se enviaban bytes de forma contínua.


class Buffer(Elaboratable):
    def __init__(self, BAUD=B115200, ROMFILE="bufferini.list", AW=4, DW=8):
        self.rx = Signal()
        self.tx = Signal()
        self.debug = Signal()
        self.leds = Signal(4)

        self.baud = BAUD
        self.romfile = ROMFILE
        self.aw = AW
        self.dw = DW

    def elaborate(self, platform):
        m = Module()

        # Memoria RAM
        addr = Signal(self.aw)

        m.submodules.ram = ram = Genram(
            AW=self.aw, DW=self.dw, ROMFILE=self.romfile)
        m.d.comb += ram.addr.eq(addr)

        # Contador
        # counter enable: Se pone a 1 cuando haya que acceder a la siguiente
        # posicion de memoria
        cena = Signal()

        with m.If(ResetSignal() == 1):
            m.d.sync += addr.eq(0)
        with m.Elif(cena):
            m.d.sync += addr.eq(addr+1)

        # Overflow del contador: se pone a uno cuando todos sus bits
        # esten a 1
        ov = Signal()

        with m.If(addr.all()):
            m.d.comb += ov.eq(1)
        with m.Else():
            m.d.comb += ov.eq(0)

        # TRANSMISOR SERIE
        m.submodules.tx0 = tx0 = Uart_tx(BAUD=self.baud)
        m.d.comb += self.tx.eq(tx0.tx)

        # RECEPTOR SERIE
        m.submodules.rx0 = rx0 = Uart_rx(BAUD=self.baud)
        m.d.comb += rx0.rx.eq(self.rx)

        m.d.comb += self.leds.eq(rx0.data[:4])
        m.d.comb += ram.data_in.eq(rx0.data)
        m.d.comb += tx0.data.eq(ram.data_out)

        # -------------------------------------
        # -- CONTROLADOR
        # -------------------------------------

        with m.FSM(reset="TX_WAIT") as fsm:
            # Esperar a que transmisor este listo para enviar
            with m.State("TX_WAIT"):
                m.d.comb += ram.rw.eq(1)
                m.d.comb += cena.eq(0)
                m.d.comb += tx0.start.eq(0)
                m.d.comb += self.debug.eq(0)

                with m.If(tx0.ready):
                    m.next = "TX_READ"
            # Lectura del dato en memoria y transmision serie
            with m.State("TX_READ"):
                m.d.comb += ram.rw.eq(1)
                m.d.comb += tx0.start.eq(1)
                m.d.comb += cena.eq(1)
                m.d.comb += self.debug.eq(0)

                # Si es el último caracter pasar a recepción
                with m.If(ov):
                    m.next = "RX_WAIT"
                with m.Else():
                    m.next = "TX_WAIT"

            # Esperar a que lleguen caracteres por el puerto serie
            with m.State("RX_WAIT"):
                m.d.comb += self.debug.eq(1)
                m.d.comb += ram.rw.eq(1)
                m.d.comb += tx0.start.eq(0)
                m.d.comb += cena.eq(0)

                with m.If(rx0.rcv):
                    m.next = "RX_WRITE"

            # Escritura de dato en memoria
            with m.State("RX_WRITE"):
                m.d.comb += ram.rw.eq(0)
                m.d.comb += cena.eq(1)
                m.d.comb += tx0.start.eq(0)
                m.d.comb += self.debug.eq(0)
                # Si es el ultimo caracter, ir al estado inicial
                with m.If(ov):
                    m.next = "TX_WAIT"
                with m.Else():
                    m.next = "RX_WAIT"

        return m

    def ports(self):
        return [self.rx, self.tx, self.debug, self.leds]


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":
    m = Module()

    subModulo = Buffer()
    m.submodules += subModulo
    platform = ICE40Up5KBEvnPlatform()
    # Como se utiliza el dominio síncrono por defecto, y en la configuración
    # de la placa se indica el reloj por defecto. No hace falta indicarselo ahora.
    # Invierte el orden de los leds para que MSB sea el rojo.
    m.d.comb += platform.request("rgb_led").eq(subModulo.leds[::-1])
    uart = platform.request("uart")
    m.d.comb += uart.tx.eq(subModulo.tx)  # 38B Header B
    m.d.comb += subModulo.rx.eq(uart.rx)  # 51A Header B
    m.d.comb += platform.request("pin42b").eq(subModulo.debug)

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
