# -*- coding: utf-8 -*-
# File: rom16x4.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, February 11th 2020, 1:51:02 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *


class Rom16x4(Elaboratable):
    def __init__(self):
        self.addr = Signal(4)
        self.data = Signal(4)

    def elaborate(self, platform):
        m = Module()

        # Memoria creada con la ayuda de nMiguen
        # Se incluye los ocho primeros valores iniciales de la memoria.
        mem = Memory(width=4, depth=16, init=[0x1, 0x2, 0x4, 0x8, 0x1,
                                              0x8, 0x4, 0x2, 0x1, 0xf,
                                              0x0, 0xf, 0xc, 0x3, 0xc,
                                              0x3])

        # Como es memoria ROM, se le asigna solamente el puerto de lectura.
        m.submodules.read_port = read_port = mem.read_port()
        m.d.comb += [read_port.addr.eq(self.addr),
                     self.data.eq(read_port.data)]

        return m

    def ports(self):
        return [self.addr, self.data]
