# -*- coding: utf-8 -*-
# File: counter_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, January 14th 2020, 5:46:44 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from counter import Counter

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.counter = counter = Counter()

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    counter_check = Signal(26)

    def process():
        data = yield counter.data
        if(data != 0):
            print("ERROR: contador no iniciado a 0.")

        for _ in range(100):   # Cuenta 100 ciclos de reloj
            data = yield counter.data
            check = yield counter_check
            if(data != check):
                print(f"ERROR: Se esperaba {check}, y se obtuvo {data}.")

            yield counter_check.eq(counter_check+1)
            # Se necesita un ciclo de reloj para que el cambio tenga efecto.
            yield Tick()  # O simplemente yield

    sim.add_sync_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/counter_tb.vcd", traces=counter.ports()):
        sim.run()
