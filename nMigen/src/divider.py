# -*- coding: utf-8 -*-
# File: divider.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Wednesday, January 29th 2020, 1:50:26 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform

F_1Hz = 12_000_000
F_2Hz = 6_000_000

F_4Mhz = 3
F_3MHz = 4
F_2MHz = 6
F_1Mhz = 12
F_1_2Mhz = 10

F_4KHz = 3_000
F_3KHz = 4_000
F_2KHz = 6_000
F_1KHz = 12_000

T_1s = 12000000
T_500ms = 6000000
T_250ms = 3000000

# ------- Frecuencias para notas musicales
# -- Octava: 0
DO_0 = 733873  # -- 16.352 Hz
DOs_0 = 692684  # -- 17.324 Hz
RE_0 = 653807  # -- 18.354 Hz
REs_0 = 617111  # -- 19.445 Hz
MI_0 = 582476  # -- 20.602 Hz
FA_0 = 549784  # -- 21.827 Hz
FAs_0 = 518927  # -- 23.125 Hz
SOL_0 = 489802  # -- 24.500 Hz
SOLs_0 = 462311  # -- 25.957 Hz
LA_0 = 436364  # -- 27.500 Hz
LAs_0 = 411872  # -- 29.135 Hz
SI_0 = 388756  # -- 30.868 Hz
# -- Octava: 1
DO_1 = 366937  # -- 32.703 Hz
DOs_1 = 346342  # -- 34.648 Hz
RE_1 = 326903  # -- 36.708 Hz
REs_1 = 308556  # -- 38.891 Hz
MI_1 = 291238  # -- 41.203 Hz
FA_1 = 274892  # -- 43.654 Hz
FAs_1 = 259463  # -- 46.249 Hz
SOL_1 = 244901  # -- 48.999 Hz
SOLs_1 = 231156  # -- 51.913 Hz
LA_1 = 218182  # -- 55.000 Hz
LAs_1 = 205936  # -- 58.270 Hz
SI_1 = 194378  # -- 61.735 Hz
# -- Octava: 2
DO_2 = 183468  # -- 65.406 Hz
DOs_2 = 173171  # -- 69.296 Hz
RE_2 = 163452  # -- 73.416 Hz
REs_2 = 154278  # -- 77.782 Hz
MI_2 = 145619  # -- 82.407 Hz
FA_2 = 137446  # -- 87.307 Hz
FAs_2 = 129732  # -- 92.499 Hz
SOL_2 = 122450  # -- 97.999 Hz
SOLs_2 = 115578  # -- 103.826 Hz
LA_2 = 109091  # -- 110.000 Hz
LAs_2 = 102968  # -- 116.541 Hz
SI_2 = 97189  # -- 123.471 Hz
# -- Octava: 3
DO_3 = 91734  # -- 130.813 Hz
DOs_3 = 86586  # -- 138.591 Hz
RE_3 = 81726  # -- 146.832 Hz
REs_3 = 77139  # -- 155.563 Hz
MI_3 = 72809  # -- 164.814 Hz
FA_3 = 68723  # -- 174.614 Hz
FAs_3 = 64866  # -- 184.997 Hz
SOL_3 = 61226  # -- 195.998 Hz
SOLs_3 = 57789  # -- 207.652 Hz
LA_3 = 54545  # -- 220.000 Hz
LAs_3 = 51484  # -- 233.082 Hz
SI_3 = 48594  # -- 246.942 Hz
# -- Octava: 4
DO_4 = 45867  # -- 261.626 Hz
DOs_4 = 43293  # -- 277.183 Hz
RE_4 = 40863  # -- 293.665 Hz
REs_4 = 38569  # -- 311.127 Hz
MI_4 = 36405  # -- 329.628 Hz
FA_4 = 34361  # -- 349.228 Hz
FAs_4 = 32433  # -- 369.994 Hz
SOL_4 = 30613  # -- 391.995 Hz
SOLs_4 = 28894  # -- 415.305 Hz
LA_4 = 27273  # -- 440.000 Hz
LAs_4 = 25742  # -- 466.164 Hz
SI_4 = 24297  # -- 493.883 Hz
# -- Octava: 5
DO_5 = 22934  # -- 523.251 Hz
DOs_5 = 21646  # -- 554.365 Hz
RE_5 = 20431  # -- 587.330 Hz
REs_5 = 19285  # -- 622.254 Hz
MI_5 = 18202  # -- 659.255 Hz
FA_5 = 17181  # -- 698.456 Hz
FAs_5 = 16216  # -- 739.989 Hz
SOL_5 = 15306  # -- 783.991 Hz
SOLs_5 = 14447  # -- 830.609 Hz
LA_5 = 13636  # -- 880.000 Hz
LAs_5 = 12871  # -- 932.328 Hz
SI_5 = 12149  # -- 987.767 Hz
# -- Octava: 6
DO_6 = 11467  # -- 1046.502 Hz
DOs_6 = 10823  # -- 1108.731 Hz
RE_6 = 10216  # -- 1174.659 Hz
REs_6 = 9642  # -- 1244.508 Hz
MI_6 = 9101  # -- 1318.510 Hz
FA_6 = 8590  # -- 1396.913 Hz
FAs_6 = 8108  # -- 1479.978 Hz
SOL_6 = 7653  # -- 1567.982 Hz
SOLs_6 = 7224  # -- 1661.219 Hz
LA_6 = 6818  # -- 1760.000 Hz
LAs_6 = 6436  # -- 1864.655 Hz
SI_6 = 6074  # -- 1975.533 Hz
# -- Octava: 7
DO_7 = 5733  # -- 2093.005 Hz
DOs_7 = 5412  # -- 2217.461 Hz
RE_7 = 5108  # -- 2349.318 Hz
REs_7 = 4821  # -- 2489.016 Hz
MI_7 = 4551  # -- 2637.020 Hz
FA_7 = 4295  # -- 2793.826 Hz
FAs_7 = 4054  # -- 2959.955 Hz
SOL_7 = 3827  # -- 3135.963 Hz
SOLs_7 = 3612  # -- 3322.438 Hz
LA_7 = 3409  # -- 3520.000 Hz
LAs_7 = 3218  # -- 3729.310 Hz
SI_7 = 3037  # -- 3951.066 Hz
# -- Octava: 8
DO_8 = 2867  # -- 4186.009 Hz
DOs_8 = 2706  # -- 4434.922 Hz
RE_8 = 2554  # -- 4698.636 Hz
REs_8 = 2411  # -- 4978.032 Hz
MI_8 = 2275  # -- 5274.041 Hz
FA_8 = 2148  # -- 5587.652 Hz
FAs_8 = 2027  # -- 5919.911 Hz
SOL_8 = 1913  # -- 6271.927 Hz
SOLs_8 = 1806  # -- 6644.875 Hz
LA_8 = 1705  # -- 7040.000 Hz
LAs_8 = 1609  # -- 7458.620 Hz
SI_8 = 1519  # -- 7902.133 Hz
# -- Octava: 9
DO_9 = 1433  # -- 8372.018 Hz
DOs_9 = 1353  # -- 8869.844 Hz
RE_9 = 1277  # -- 9397.273 Hz
REs_9 = 1205  # -- 9956.063 Hz
MI_9 = 1138  # -- 10548.082 Hz
FA_9 = 1074  # -- 11175.303 Hz
FAs_9 = 1014  # -- 11839.822 Hz
SOL_9 = 957  # -- 12543.854 Hz
SOLs_9 = 903  # -- 13289.750 Hz
LA_9 = 852  # -- 14080.000 Hz
LAs_9 = 804  # -- 14917.240 Hz
SI_9 = 759  # -- 15804.266 Hz
# -- Octava: 10
DO_10 = 717  # -- 16744.036 Hz
DOs_10 = 676  # -- 17739.688 Hz
RE_10 = 638  # -- 18794.545 Hz
REs_10 = 603  # -- 19912.127 Hz
MI_10 = 569  # -- 21096.164 Hz
FA_10 = 537  # -- 22350.607 Hz
FAs_10 = 507  # -- 23679.643 Hz
SOL_10 = 478  # -- 25087.708 Hz
SOLs_10 = 451  # -- 26579.501 Hz
LA_10 = 426  # -- 28160.000 Hz
LAs_10 = 403  # -- 29834.481 Hz
SI_10 = 380  # -- 31608.531 Hz


class Divider(Elaboratable):
    def __init__(self, M=F_1Hz):
        self.clk_in = Signal()
        self.clk_out = Signal()

        self.m = M

    def elaborate(self, platform):
        m = Module()

        # Crea una señal con el número de bits necesarios para el divisor.
        N = Signal(range(self.m))

        m.domains.sync = ClockDomain(name="div")
        m.d.comb += ClockSignal("div").eq(self.clk_in)

        # Registro
        divcounter = Signal.like(N)

        # Contador de módulo M
        with m.If(divcounter == (self.m-1)):
            m.d.div += divcounter.eq(0)
        with m.Else():
            m.d.div += divcounter.eq(divcounter+1)

        # Salida del módulo
        m.d.comb += self.clk_out.eq(divcounter[-1])

        return m

    def ports(self):
        return [self.clk_in, self.clk_out]
