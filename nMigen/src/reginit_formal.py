# -*- coding: utf-8 -*-
# File: reginit_formal.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Saturday, January 25th 2020, 3:59:16 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.asserts import Assert, Assume, Cover
from nmigen.asserts import Past, Rose, Fell, Stable
from nmigen.cli import main_parser, main_runner
from reginit import Reginit


if __name__ == "__main__":
    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.reginit = reginit = Reginit(NP=1)

    # La señal de inicio válido se pone a 1 en el primer ciclo.
    inicio_val = Signal()
    m.d.pres += inicio_val.eq(1)

    # Antes del primer ciclo, data tiene que tener el valor 0
    with m.If(inicio_val == 0):
        m.d.comb += Assert(reginit.data == 0)

    # Una vez pasado el ciclo inicial, comprueba que data tiene un valor distinto
    # en cada ciclo.
    with m.If(inicio_val):
        m.d.pres += Assert(~Stable(reginit.data))
        # Data solo puede tener el valor 0b1100 o 0b0011
        m.d.comb += Assert((reginit.data == 0b1100) |
                           (reginit.data == 0b0011))

    main_runner(parser, args, m, ports=reginit.ports())
