# -*- coding: utf-8 -*-
# File: mux2_formal.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, January 24th 2020, 3:22:51 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.asserts import Assert, Assume, Cover
from nmigen.asserts import Past, Rose, Fell, Stable
from nmigen.cli import main_parser, main_runner
from mux2 import Mux2


if __name__ == "__main__":
    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.mux2 = mux2 = Mux2(NP=1)

    # La señal de inicio válido se pone a 1 en el primer ciclo.
    inicio_val = Signal()
    m.d.sync += inicio_val.eq(1)

    # Antes del primer ciclo, data tiene que tener el valor 0
    with m.If(inicio_val == 0):
        m.d.comb += Assert(mux2.data == mux2.val0)

    # Una vez pasado el ciclo inicial, comprueba que cuando data tiene el valor 1,
    # anteriormente tenía el valor 0.
    with m.If((mux2.data == mux2.val1) & (inicio_val)):
        m.d.comb += Assert(Past(mux2.data) == mux2.val0)

    # La salida solo puede tomar el valor 0 o 1
    m.d.comb += Assert((mux2.data == mux2.val0) | (mux2.data == mux2.val1))

    main_runner(parser, args, m, ports=mux2.ports())
