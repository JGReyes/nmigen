# -*- coding: utf-8 -*-
# File: tristate1.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, February 20th 2020, 11:28:18 am
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from divider2 import Divider2, T_1s
from nmigen.compat.fhdl.specials import TSTriple


# NOTA: Al no existir documentación, ni ejemplos actualmente sobre el uso de
# triestados en nMigen, se ha utilizado la capa de compatibilidad con Migen
# para crear un objeto triestado. Después de leer partes del código fuente y
# varias pruebas solo se ha conseguido hacerlo funcionar al indicarle el pin
# dentro del metodo elaborate.

# Debido a que se usa el pin del hardware real, este modulo no se puede simular
# dado que falla el simulador con un error: UnusedElaboratable.
# En la documentación de su predecesor (Migen) se indica lo siguiente:
# Like modern FPGA architectures, Migen does not support internal tri-states.

# Debido a lo anterior, solo se ha realizado este ejercicio entorno a los triestados.

class Tristate1(Elaboratable):
    def __init__(self, DELAY=T_1s):

        self.delay = DELAY

    def elaborate(self, platform):
        m = Module()

        # Biestable que está siempre a 1
        reg1 = Signal()
        m.d.sync += reg1.eq(1)

        # Se solicita el pin a utilizar por el triestado.
        platform = ICE40Up5KBEvnPlatform()
        led0 = platform.request("pin42b")
        # Se crea el objeto triestado de Migen
        ts = TSTriple()
        # Se asocia ts con la salida del buffer triestado del pin a usar en la placa.
        m.submodules.tri = tri = ts.get_tristate(led0.o)

        # Divisor para temporizacion
        m.submodules.ch0 = ch0 = Divider2(M=self.delay)

        # Se le asigna la salida del triestado al valor del registro reg1.
        m.d.comb += tri.o.eq(reg1)
        # Se controla la salida del triestado con la entrada oe (output enable) del mismo.
        m.d.comb += tri.oe.eq(ch0.clk_out)

        return m

    def ports(self):
        return []


# Sintesis y programación en la FPGA
# Los archivos resultantes se crean en la carpeta build.
if __name__ == "__main__":
    m = Module()

    subModulo = Tristate1()
    m.submodules += subModulo

    platform = ICE40Up5KBEvnPlatform()

    # Poner do_program a False si solo se quiere generar los ficheros.
    # Cambiar la opción sram a False para programar la flash.
    # NOTA: en la placa hay que cambiar los puentes J6 para elegir entre
    # arranque en FLASH o ICE (sram de la FPGA). Si se elige programar la
    # flash, los puentes J6 no tienen efecto, siempre queda programada. Si
    # se elige la sram, el puente J6 tiene que estar en ICE o será incapaz
    # de comunicarse con la FPGA, sin dar ningún mensaje de error, haciendo
    # creer que hay algún fallo en el diseño.
    platform.build(m, do_program=True, program_opts={"sram": True})

    # Solamente si se quiere generar el archivo verilog.
    def generate_verilog():
        from nmigen.back import verilog
        import sys
        import os

        directorio = ''.join([os.getcwd(), os.sep, "output", os.sep])
        archivo = ''.join([sys.argv[0].split(os.sep)[-1][:-2], "v"])
        ruta = ''.join([directorio, archivo])

        with open(ruta, "w") as verilog_file:
            verilog_file.writelines(
                verilog.convert(m, ports=subModulo.ports()))

    # Descomentar la siguiente línea para genererar un archivo verilog
    # y poder crear un esquema mediante Generate schematic. El archivo se
    # encuentra en la carpeta output con el nombre del archivo python.

    # generate_verilog()
