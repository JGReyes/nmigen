# -*- coding: utf-8 -*-
# File: divider2.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Thursday, February 6th 2020, 3:49:05 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform

F_1Hz = 12_000_000
F_2Hz = 6_000_000

F_4Mhz = 3
F_3MHz = 4
F_2MHz = 6
F_1Mhz = 12
F_1_2Mhz = 10

F_8Khz = 1_500
F_4KHz = 3_000
F_3KHz = 4_000
F_2KHz = 6_000
F_1KHz = 12_000

T_1s = 12_000_000
T_500ms = 6_000_000
T_250ms = 3_000_000
T_200ms = 2_400_000
T_100ms = 1_200_000


# Siguiendo las reglas de diseño síncrono y dado que nMigen crea un dominio
# síncrono por defecto (sync) para el reloj principal. Se utiliza éste lo máximo
# posible, eliminando las entradas de reloj de los siguientes circuitos.

class Divider2(Elaboratable):
    def __init__(self, M=F_1Hz, PULSE=False):
        self.clk_out = Signal()

        self.m = M
        self.pulse = PULSE

    def elaborate(self, platform):
        m = Module()

        # Registro
        # Crea una señal con el número de bits necesarios para el divisor.
        divcounter = Signal(range(self.m))

        # Contador de módulo M
        with m.If(divcounter == (self.m-1)):
            m.d.sync += divcounter.eq(0)
        with m.Else():
            m.d.sync += divcounter.eq(divcounter+1)

        # Si PULSE es true, la salida solo genera un pulso de 1 ciclo de reloj.
        # En caso contrario saca siempre el bit más significativo.

        # Salida del módulo
        if (self.pulse):
            with m.If(divcounter == 0):
                m.d.comb += self.clk_out.eq(1)
            with m.Else():
                m.d.comb += self.clk_out.eq(0)
        else:
            m.d.comb += self.clk_out.eq(divcounter[-1])

        return m

    def ports(self):
        return [self.clk_out]
