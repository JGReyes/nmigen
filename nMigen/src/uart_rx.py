# -*- coding: utf-8 -*-
# File: uart_rx.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Friday, February 7th 2020, 3:56:58 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.build import Platform
from ice40_up5k_b_evn import ICE40Up5KBEvnPlatform
from baudgen_rx import *


class Uart_rx(Elaboratable):
    def __init__(self, BAUD=B115200):
        self.rx = Signal()     # -- Línea de recepción serie
        self.rcv = Signal()    # -- Indica dato recibido
        self.data = Signal(8)  # -- Dato recibido

        self.baud = BAUD

    def elaborate(self, platform):
        m = Module()

        # Linea de recepcion registrada
        rx_r = Signal()

        # Microordenes
        # Poner a cero contador de bits
        clear = Signal()
        # Cargar dato recibido
        load = Signal()
        # Activar señal de reloj de datos
        bauden = Signal()

        # -------------------------------------
        # -- RUTA DE DATOS
        # -------------------------------------

        # Registrar la señal de recepcion de datos
        m.d.sync += rx_r.eq(self.rx)

        # Generador de reloj para la llegada de datos.
        m.submodules.baudgen0 = baudgen0 = Baudgen_rx(M=self.baud)
        m.d.comb += baudgen0.clk_ena.eq(bauden)

        # Contador de bits
        bitc = Signal(4)

        # Registro de desplazamiento para almacenar los bits recibidos
        raw_data = Signal(10)

        with m.If(clear):
            m.d.sync += bitc.eq(0)
        with m.Elif(baudgen0.clk_out == 1):
            m.d.sync += bitc.eq(bitc + 1)
            m.d.sync += raw_data.eq(Cat(raw_data[1:], rx_r))

        # Registro de datos. Almacenar el dato recibido
        # Se utiliza la señal por defecto de reset que crea nMigen para el dominio sync.
        with m.If(ResetSignal() == 1):
            m.d.sync += self.data.eq(0)
        with m.Elif(load):
            m.d.sync += self.data.eq(raw_data[1:9])

        # -------------------------------------
        # -- CONTROLADOR
        # -------------------------------------

        with m.FSM(reset="IDLE") as fsm:
            # Estado de reposo. Al llegar el bit de start se pasa al estado siguiente
            with m.State("IDLE"):
                m.d.comb += bauden.eq(0)
                m.d.comb += clear.eq(1)
                m.d.comb += load.eq(0)
                m.d.comb += self.rcv.eq(0)
                with m.If(rx_r == 0):
                    m.next = "RECV"
            # Recibiendo datos
            with m.State("RECV"):
                m.d.comb += bauden.eq(1)
                m.d.comb += clear.eq(0)
                m.d.comb += load.eq(0)
                m.d.comb += self.rcv.eq(0)
                # Vamos por el ultimo bit: pasar al siguiente estado
                with m.If(bitc == 10):
                    m.next = "LOAD"
            # Almacenamiento del dato
            with m.State("LOAD"):
                m.d.comb += bauden.eq(0)
                m.d.comb += clear.eq(0)
                m.d.comb += load.eq(1)
                m.d.comb += self.rcv.eq(0)
                m.next = "DAV"
            # Señalizar dato disponible
            with m.State("DAV"):
                m.d.comb += bauden.eq(0)
                m.d.comb += clear.eq(0)
                m.d.comb += load.eq(0)
                m.d.comb += self.rcv.eq(1)
                m.next = "IDLE"

        return m

    def ports(self):
        return [self.rx, self.rcv, self.data]
