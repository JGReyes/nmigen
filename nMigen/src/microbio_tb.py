# -*- coding: utf-8 -*-
# File: microbio_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, February 25th 2020, 12:06:32 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle, Tick
from microbio import Microbio

# Simulación
if __name__ == "__main__":

    # Para la simulacion se usa un WAIT_DELAY de 3 ciclos de reloj
    WAIT_DELAY = 3
    ROMFILE = "prog.list"

    m = Module()
    m.submodules.microbio = microbio = Microbio(
        WAIT_DELAY=WAIT_DELAY, ROMFILE=ROMFILE)

    rstn_ini = Signal()
    m.d.comb += microbio.rstn_ini.eq(rstn_ini)

    sim = Simulator(m)
    sim.add_clock(83.33e-9)  # Reloj a 12 Mhz

    def reset():
        yield Delay(83.33e-9 * 2)  # 2 ciclos de reloj
        yield rstn_ini.eq(1)
        yield Settle()

    def process():
        for _ in range(161):
            yield Tick()

        print("FIN de la simulación")

    sim.add_process(reset)
    sim.add_sync_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/microbio_tb.vcd", traces=microbio.ports()):
        sim.run()
