# -*- coding: utf-8 -*-
# File: fport_tb.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Tuesday, January 14th 2020, 12:05:04 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.back.pysim import Simulator, Delay, Settle
from fport import Fport

# Simulación
if __name__ == "__main__":
    m = Module()
    m.submodules.fport = fport = Fport()

    sim = Simulator(m)

# Pasadas 10 microsegundos comprobamos si el cable tiene el patrón establecido.

    def process():
        yield Delay(10e-6)
        # fuerza a todas las computaciones combinacionales ha realizarse.
        yield Settle()
        # Se pasa el valor de la señal s a una variable de python, para poder
        # compararse directamente.
        salida = yield fport.data

        if (salida != 0b1010):
            print("---->¡ERROR! Salida erronea")
        else:
            print("Componente ok!")

    sim.add_process(process)
    # Define el fichero donde volcar los datos.
    with sim.write_vcd("output/fport_tb.vcd", traces=fport.ports()):
        sim.run()
