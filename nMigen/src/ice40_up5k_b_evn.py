# -*- coding: utf-8 -*-
# File: ice40_up5k_b_evn.py
# Project: nMigen
# Created Date: Friday, January 10th 2020, 6:03:25 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

# Plataforma para nMigen de la placa Ice40_Up5K_b_env

import os
import subprocess

from nmigen.build import *
from nmigen.vendor.lattice_ice40 import *
from nmigen_boards.resources import *


__all__ = ["ICE40Up5KBEvnPlatform"]


class ICE40Up5KBEvnPlatform(LatticeICE40Platform):
    device = "iCE40UP5K"
    package = "SG48"
    default_clk = "clk12"
    resources = [
        Resource("clk12", 0, Pins("35", dir="i"),
                 Clock(12e6), Attrs(GLOBAL=True, IO_STANDARD="SB_LVCMOS")),

        Resource("pin42b", 0, Pins("31",  dir="o"),
                 Attrs(IO_STANDARD="SB_LVCMOS")),

        RGBLEDResource(0, r="41", g="40", b="39", invert=True,
                       attrs=Attrs(IO_STANDARD="SB_LVCMOS")),

        *SwitchResources(pins="23 25 34 43", invert=False,
                         attrs=Attrs(IO_STANDARD="SB_LVCMOS")),

        # La placa no utiliza el chip FT2232H para un puerto UART.
        # Así que hay que utilizar un cable USB-a-serie.
        # Se han elegido los pines 38B y 51A para "tx" y "rx", en el conector Header B.

        UARTResource(0,
                     tx="27", rx="42",
                     attrs=Attrs(IO_STANDARD="SB_LVCMOS")),

        *SPIFlashResources(0,
                           cs="16", clk="15", mosi="14", miso="17",
                           attrs=Attrs(IO_STANDARD="SB_LVCMOS")),
    ]

    # Se usa la numeración según el esquemático de la placa.
    connectors = [
        # PMOD U6
        Connector("pmod", 0, "16 17 14 15 - - 27 26 32 31 - -"),
        # HEADER A (J52)
        Connector("header_a", 0, "- - 39 17 40 14 - 15 41 16 - -"),
        # HEADER B (J2)
        Connector("header_b", 0,
                  "- - 23 - 25 - 26 36 27 42 32 38 31 28 37 35 34 - 43 -"),
        # HEADER B (J3)
        Connector("header_c", 0,
                  "- 12 4 21 3 13 48 20 45 19 47 18 44 11 46 10 2 9 - 6"),
    ]

    def toolchain_prepare(self, fragment, name, **kwargs):
        overrides = {
            "nextpnr_opts":
                "--pcf-allow-unconstrained",
        }
        return super().toolchain_prepare(fragment, name, **overrides, **kwargs)

    def toolchain_program(self, products, name, sram=False):
        iceprog = os.environ.get("ICEPROG", "iceprog")
        with products.extract("{}.bin".format(name)) as bitstream_filename:
            if (sram):
                subprocess.check_call([iceprog, "-S", bitstream_filename])
            else:
                subprocess.check_call([iceprog, bitstream_filename])
