# -*- coding: utf-8 -*-
# File: counter_formal.py
# Project: Ejemplos del tutorial en nMigen
# Created Date: Wednesday, January 15th 2020, 5:17:49 pm
# Author: JGReyes
# e-mail: josegreyes@circuiteando.net
# web: www.circuiteando.net
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software") to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright (c) 2020 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


from nmigen import *
from nmigen.asserts import Assert, Assume, Cover
from nmigen.asserts import Past, Rose, Fell, Stable
from nmigen.cli import main_parser, main_runner
from counter import Counter


if __name__ == "__main__":
    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.counter = counter = Counter()

    # Busca casos en los que el valor del contador sea 0, durante los 40 pasos
    # o ciclos, que están cofigurados en el archivo formal.sby
    # Por lo que se creara un archivo trace0.vcd con el caso en el que el
    # contador es 0. El inicio, así como también mostraría el caso de desbordamiento
    # si en los ciclos especificados se llegara a producir.
    m.d.comb += Cover(counter.data == 0)

    # Prueba que el valor actual sea igual al valor del ciclo anterior + 1.
    # Siempre que el valor sea mayor que 0.
    with m.If(counter.data > 0):
        m.d.comb += Assert(counter.data == Past(counter.data)+1)

    main_runner(parser, args, m, ports=counter.ports())
