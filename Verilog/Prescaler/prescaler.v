module prescaler(output wire rgb0, rgb1, rgb2,
                 input wire clk_in);

    // Este ejemplo está modificado para utilizar el driver RGB que tiene
    // la fpga a modo de "Hard IP". Con este driver se puede controlar cada
    // led con una señal PWM y se puede fijar la corriente máxima a utilizar.
    // En este caso 4 mA.

    reg clk_out;
    // n de bits por defecto del prescaler
    parameter N = 25;
    // registro para el contador
    reg [N-1:0] count = 0;

    // El bit más significatido se saca por la salida
    always @(*) begin
        clk_out = count[N-1];
    end

    always @(posedge clk_in) begin
        count <= count + 1;
    end

// -------- Esta parte es solo para utilizar el driver RGB ----------------
// La simulación da error dado que no conoce el componente SB_RGBA_DRV

    // Desactivan los leds azul y rojo
    wire disable_blue = 1'b0;
    wire disable_red = 1'b0;

    // Instantiate iCE40 LED driver hard logic
    SB_RGBA_DRV RGBA_DRIVER (
        .CURREN(1'b1),
        .RGBLEDEN(1'b1),
        .RGB2PWM(disable_blue),      // Blue
        .RGB0PWM(disable_red),       // Red
        .RGB1PWM(clk_out),           // Green
        .RGB0(rgb0),
        .RGB1(rgb1),
        .RGB2(rgb2)
    );

    // Parameters from iCE40 UltraPlus LED Driver Usage Guide, pages 19-20
    // https://www.latticesemi.com/-/media/LatticeSemi/Documents/ApplicationNotes/IK/ICE40LEDDriverUsageGuide.ashx?document_id=50668

    localparam RGBA_CURRENT_MODE_FULL = "0b0";
    localparam RGBA_CURRENT_MODE_HALF = "0b1";

    // Current levels in Full / Half mode
    localparam RGBA_CURRENT_04MA_02MA = "0b000001";
    localparam RGBA_CURRENT_08MA_04MA = "0b000011";
    localparam RGBA_CURRENT_12MA_06MA = "0b000111";
    localparam RGBA_CURRENT_16MA_08MA = "0b001111";
    localparam RGBA_CURRENT_20MA_10MA = "0b011111";
    localparam RGBA_CURRENT_24MA_12MA = "0b111111";

    // Set parameters of RGBA_DRIVER (output current)
    // Mapping of RGBn to LED colours determined experimentally
    defparam RGBA_DRIVER.CURRENT_MODE = RGBA_CURRENT_MODE_HALF;
    defparam RGBA_DRIVER.RGB0_CURRENT = RGBA_CURRENT_08MA_04MA;  // Blue
    defparam RGBA_DRIVER.RGB1_CURRENT = RGBA_CURRENT_08MA_04MA;  // Red
    defparam RGBA_DRIVER.RGB2_CURRENT = RGBA_CURRENT_08MA_04MA;  // Green

endmodule