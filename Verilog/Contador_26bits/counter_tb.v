module counter_tb;

    reg clk;
    wire [25:0] counter_in;
    counter COUNT (.clk (clk), .data (counter_in));
    reg [25:0] n_count = 1;

    initial begin
        $dumpfile("output/counter_tb.vcd");
        $dumpvars(0, counter_tb);

        clk = 0; // Valor inicial para el reloj.

        if (counter_in != 0)
            $display("ERROR: contador no iniciado a 0");
    end

    always begin
        #1 clk <= ~clk;
    end

    always @(negedge clk) begin
        if (counter_in != n_count)
             $display("ERROR: Se esperaba %d, y se obtuvo %d", n_count, counter_in);

        n_count <= n_count +1;

        if (n_count >= 100)
            $finish;
    end

endmodule // counter_tb