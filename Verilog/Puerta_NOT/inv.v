module inv(output wire B,
           input wire A);

    assign B = ~A;

endmodule

// Atención:
// En esta fpga la lógica del led conectado a B esta invertida por lo que 1
// es apagado y 0 encencido. Volviendo a invertir la lógica.