module inv_tb;

    wire dout;
    reg din;
    inv NP1 (.B (dout), .A (din));

    initial begin
        $dumpfile("output/inv_tb.vcd");
        $dumpvars(0, inv_tb);

        din = 1'b1;
        #10 if (dout != 1'b0)
                $display("---->!ERROR¡ Salida Erronea");

        din = 1'b0;
        #10 if (dout != 1'b1)
                $display("---->!ERROR¡ Salida Erronea");
            else
                $display("Componente OK");

        #10 $finish;
    end
endmodule // inv_tb