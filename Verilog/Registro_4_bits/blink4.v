
module prescaler(input wire clk_in, output wire clk_out);

    //-- Numero de bits del prescaler (por defecto)
    parameter N = 22;

    //-- Registro para implementar contador de N bits
    reg [N-1:0] count = 0;

    //-- El bit más significativo se saca por la salida
    assign clk_out = count[N-1];

    //-- Contador: se incrementa en flanco de subida
    always @(posedge(clk_in)) begin
      count <= count + 1;
    end
endmodule

module blink4(input wire clk,           //--Reloj
              output wire [3:0] data);    //-- Salida del registro

    //-- Bits para el prescaler
    parameter N = 22;

    //-- Reloj principal (prescalado)
    wire clk_base;

    //-- Datos del registro
    reg [3:0] dout = 0;

    //-- Cable de entrada al registro
    wire [3:0] din;

    //-- Instanciar el prescaler
    prescaler #(.N(N))
      PRES (
        .clk_in(clk),
        .clk_out(clk_base)
      );

    //-- Registro
    always @(posedge(clk_base))
      dout <= din;

    //-- Puerta NOT entra la salida y la entrada
    assign din = ~dout;

    //-- Sacar datos del registro por la salida
    assign data = dout;

endmodule