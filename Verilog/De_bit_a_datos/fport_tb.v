module fport_tb(input wire [3:0] DATA);

    fport FP1 (.data (DATA));
    initial begin
        $dumpfile("output/fport_tb.vcd");
        $dumpvars(0, fport_tb);

        #10 if (DATA != 4'b1010)
                $display("---->!ERROR¡ Salida Erronea");
            else
                $display("Componente OK");

        #10 $finish;
    end
endmodule // fport_tb